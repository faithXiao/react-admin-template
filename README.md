# react-admin-template

<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->

<!-- code_chunk_output -->

- [react-admin-template](#react-admin-template)
  - [安装](#安装)
  - [路由管理](#路由管理)
  - [业务组件](#业务组件)
    - [Alert确认提示框](#alert确认提示框)
    - [Toask轻提示](#toask轻提示)
    - [Loading加载框](#loading加载框)
    - [Dialog弹出层](#dialog弹出层)
    - [Button按钮](#button按钮)
    - [Tag标签](#tag标签)
    - [Table表格](#table表格)
    - [Pagination分页器](#pagination分页器)
    - [Form](#form)
      - [input输入框](#input输入框)
      - [radio单选框](#radio单选框)
      - [checkbox复选框](#checkbox复选框)
      - [select选择框](#select选择框)
      - [switch开关](#switch开关)
      - [timePicker时间选择器](#timepicker时间选择器)
      - [calendar日历选择器](#calendar日历选择器)
      - [upload文件上传](#upload文件上传)
      - [Form表单](#form表单)
        - [表单验证](#表单验证)
    - [Tree树形展开](#tree树形展开)
    - [Crop图片裁剪](#crop图片裁剪)
    - [Tooltip提示](#tooltip提示)
    - [Carousel跑马灯](#carousel跑马灯)

<!-- /code_chunk_output -->

## 安装
```
# 克隆项目
git clone https://gitee.com/faithXiao/react-admin-template.git

# 进入项目目录
cd react-admin-template

# 安装依赖
npm install

# 启动服务
npm run start

# 线上地址
http://42.193.144.245/react-admin-template
```

## 路由管理
```
src/router/index.js

import React from 'react'
import Layout from '../views/layout'

const routes = [
    {
        path: "/",  //路径
        redirect: '/home',  //重定向地址
        hidden: true  //侧边栏隐藏
    },
    {
        path: "/home",
        redirect: '/home/index',
        component: Layout,  //公共页面组件（头部，侧边栏，页面主体）布局
        name: 'home',  //页面名称
        children: [
            {
                path: "index",
                component: React.lazy(() => import(/* webpackChunkName: "home" */ '../views/home/index')),  //页面主体
                name: 'home'
            }
        ]
    },
    {
        path: "/another",
        component: Layout,
        name: '一级',
        children: [
            {
                path: 'a',
                component: React.lazy(() => import(/* webpackChunkName: "another-a" */ '../views/another/a/index')),
                name: '二级',
                children: [
                    {
                        path: '1',
                        component: React.lazy(() => import(/* webpackChunkName: "another-a-1" */ '../views/another/a/a1/index')),
                        name: '三级-1'
                    },
                    
                    {
                        path: '2',
                        component: React.lazy(() => import(/* webpackChunkName: "another-a-2" */ '../views/another/a/a2/index')),
                        name: '三级-2'
                    }
                ]
            },
            {
                path: 'b',
                component: React.lazy(() => import(/* webpackChunkName: "another-b" */ '../views/another/b/index')),
                name: '二级'
            },
            {
                path: 'params/:id',
                component: React.lazy(() => import(/* webpackChunkName: "another" */ '../views/another/params/index')),
                name: '二级（路由参数）',
                hidden: true
            }
        ]
    },
    {
        path: "/404",  //404页面
        component: React.lazy(() => import(/* webpackChunkName: "404" */ '../views/404/index')),
        name: '404',
        hidden: true
    },
    {
        path: "*",  //url地址全部匹配不成功重定向404
        redirect: '/404',
        hidden: true
    }
]
```

## 业务组件

### Alert确认提示框
```
import { Alert } from '@/component/index'

let alert = Alert()
alert.showAlert({
   title: '提示',  //标题
   message: 'Alert',  //提示消息
   confirm: () => {  //确认回调

   },
   cancel: () => {  //取消，关闭回调

   }
})
```

### Toask轻提示
```
import { Toask } from '@/component/index'

let toask = Toask()
toask.showToask({
    type: 'success',  类型：success info warning error
    title: 'success',  提示标题
    duration: 1500  显示时长ms，若为0不自动关闭，有"x"关闭按钮可手动关闭
})
```

### Loading加载框
```
import { Loading } from '@/component/index'

let loading = Loading()
loading.showLoading('加载中...')  //显示
setTimeout(() => {
    loading.hideLoading()  //关闭
}, 1500)
```

### Dialog弹出层
```
import { Dialog } from '@/component/index'

constructor(props){
    super(props)
    this.state = {
        showDialog: false
    }
}
open(){
    this.setState({
        showDialog: true
    })
}
close(){
    this.setState({
        showDialog: false
    })
}
render(){
    return(
        <div>
            <Dialog
                title="Dialog"  //标题
                width="40%"  //模态框宽度，% || px
                show={ this.state.showDialog }  //控制Dialog显示隐藏
                close={ (() => this.setState({ showDialog: false })).bind(this) }  //在Dialog内部关闭，需传入该关闭的回调函数
                destroy={ true }  //关闭Dialog是否销毁。若Dialog内存在子组件，则该子组件会触发卸载生命周期。show为true只是隐藏Dialog，不会在页面上销毁
            >
                <button onClick={ this.close.bind(this) }>close Dialog</button>
            </Dialog>
            <button onClick={ this.open.bind(this) }>open Dialog</button>
        </div>
    )
}
```

### Button按钮
```
import { Button } from '@/component/index'

render(){
    return(
        <Button
            type="primary"  //类型：primary success error warning info
            size="small"  //大小：mini small medium
            pain  //朴素按钮
        >按钮</Button>
    )
}
```

### Tag标签
```
import { Tag } from '@/component/index'

render(){
    return(
        <Tag
            type="success"  //类型：success error warning info primary
            size="small"  //大小：mini small medium
        >标签</Tag>
    )
}

```

### Table表格
```
import { Table } from '@/component/index'

constructor(props){
    super(props)
    this.state = {
        data: [
            {
                id: 1,
                name: '张三',
                sex: '男',
                age: '18',
                address: '广东'
            },
            {
                id: 2,
                name: '李四',
                sex: '女',
                age: '20',
                address: '上海'
            }
        ],
        loading: false,
        multipleSelectList: []
    }
    this.tableRef = React.createRef()
}
getItem(item, index){
    console.log(item, index)
}
multipleSelect(value){
    this.setState(state => {
        state.multipleSelectList = value
        return state
    })
}
clearSelection(){  //清除表格多选
    this.tableRef.current.clearSelection()
}
render(){
    return(
        <Table.table
            data={ this.state.data }  //数据
            align="center"  //表格对齐方式left center right，默认left
            selectionChange={ this.multipleSelect.bind(this) }  //checkbox选中回调
            multipleSelectList={ this.state.multipleSelectList }
            loading={ this.state.loading }  //数据加载loading
            ref={ this.tableRef }
            rowKey="id"  //若使用展开行，多选框功能，添加数据唯一标识属性
        >
            //展开行在表格数据动态更新的时候不会自动消失(数据添加，删除，分页切换等)，需调用clearExpand()手动清除
            <Table.column
                type="expand"  //行展开
                render={  //展开渲染函数
                    (item, index) => {  //返回数组每一项
                        return <div style={{ textAlign: 'left' }}>...</div>  //注：展开节点文字对齐方式会继承table对齐方式
                    }
                }
            />
            <Table.column
                type="expand"
                lazy={ true }  //异步获取数据展开，设置lazy
                render={
                    (item, index) => {
                        return new Promise(resolve => {  //返回一个Promise对象
                            setTimeout(() => {
                                resolve(
                                    <div>...</div>
                                )
                            }, 1000)
                        })
                    }
                }
            />
            <Table.column type="selection" />  //checkbox选择框，设置type="selection"
            <Table.column
                prop="name"  //列所对应的值
                label="姓名"  //列名
                width="180"  //列宽，若不设置则平分容器剩余宽度（容器宽度 - 所有列已设置宽度总和）
                align="left"  //列对齐方式，不受表格对齐方式影响
            />
            <Table.column prop="sex" label="性别" width="180" />
            <Table.column prop="age" label="年龄" />
            <Table.column prop="address" label="地址" />
            <Table.column
                label="操作"
                render={  //设置函数渲染需要特殊处理的数据，如图片，按钮，Tag标签等
                    (item, index) => {  //返回当前操作的数据项item，下标index
                        return(
                            <div>
                                <button onClick={ (() => { this.getItem(item, index) }).bind(this) }>操作</Button>
                            </div>
                        )
                    }
                }
            />
        </Table.table>
    )
}
```

### Pagination分页器
```
import { Pagination } from '@/component/index'

currentChange(e){  //e: 页码
    console.log(e)
}
render(){
    return(
        <Pagination
            total={ 100 }  //总条数
            size={ 12 }  //每页显示条数
            count={ 7 }  //最大页码按钮数
            current={ 1 }  //当前页码
            currentChange={ this.currentChange.bind(this) }  //页码改变回调函数
        />
    )
}
```

### Form
```
import { Form } from '@/component/index'  //Form是一个包含所有表单组件的对象，使用表单组件前需引入该对象
```
#### input输入框
```
constructor(props){
    super(props)
    this.state = {
        value: ''
    }
}
onChange(value){
    console.log(value)
    this.setstate({
        value
    })
}
render(){
    return(
        <Form.input
            type="text"  //类型："text" "password" "textarea"
            value={ this.state.value }  //输入框绑定值
            onChange={ this.onChange.bind(this) }  //输入框值改变回调
            disabled  //禁用
            placeholder="..."
        />
    )
}
```
#### radio单选框
```
constructor(props){
    super(props)
    this.state = {
        value: ''
    }
}
onChange(value){
    console.log(value)
    this.setstate({
        value
    })
}
render(){
    return(
        <Form.radioGroup
            value={ this.state.value }  //单选框绑定值
            onChange={ this.onChange.bind(this) }  //单选框值改变回调
        >
            <Form.radio
                label="radio 1"  //单选框标签
                value="1"  //单选框值
            />
            <Form.radio label="radio 2" value="2" />
        </Form.radioGroup>
    )
}
```
#### checkbox复选框
```
constructor(props){
    super(props)
    this.state = {
        value: []
    }
}
onChange(value){
    console.log(value)
    this.setstate({
        value
    })
}
render(){
    return(
        <Form.checkboxGroup
            value={ this.state.value }  //复选框绑定值
            onChange={ this.onChange.bind(this) }  //复选框值改变回调
        >
            <Form.checkbox
                label="checkbox 1"  //复选框标签
                value="1"  //复选框值
            />
            <Form.checkbox label="checkbox 2" value="2" />
        </Form.checkboxGroup>
    )
}
```
#### select选择框
```
constructor(props){
    super(props)
    this.state = {
        value: ''  //若开启多选，则value应为数组[]
    }
}
onChange(value){
    console.log(value)
    this.setstate({
        value
    })
}
render(){
    return(
        <Form.select
            value={ this.state.value }  //绑定值
            multiple  //开启多选，value应定义为数组[]
            onChange={ this.onChange.bind(this) }  //绑定值改变回调
            placeholder="请选择..."
        >
            <Form.option
                label="option 1"  //标签
                value="1"  //值
            />
            <Form.option label="option 2" value="2" />
            <Form.option label="option 3" value="3" />
            <Form.option label="option 4" value="4" />
            <Form.option label="option 5" value="5" />
            <Form.option label="option 6" value="6" />
        </Form.select>
    )
}
```
#### switch开关
```
constructor(props){
    super(props)
    this.state = {
        value: ''
    }
}
onChange(value){
    console.log(value)
    this.setstate({
        value
    })
}
render(){
    return(
        <Form.switch
            value={ this.state.value }  //绑定值
            activeText="开"  //开启提示信息
            inactiveText="关"  //关闭提示信息
            onChange={ this.onChange.bind(this) }  //绑定值改变回调
        />
    )
}
```
#### timePicker时间选择器
```
constructor(props){
    super(props)
    this.state = {
        value: ''
    }
}
onChange(value){
    console.log(value)
    this.setstate({
        value
    })
}
render(){
    return(
        <Form.timePicker
            value={ this.state.value }  //绑定值
            onChange={ this.onChange.bind(this) }  //绑定值改变回调
            placeholder="请选择..."
        />
    )
}
```
#### calendar日历选择器
```
constructor(props){
    super(props)
    this.state = {
        value: ''  //日期对象new Date() 或 ''
    }
}
onChange(value){
    console.log(value)
    this.setstate({
        value
    })
}
render(){
    return(
        <Form.calendar
            value={ this.state.value }  //绑定值
            onChange={ this.onChange.bind(this) }  //绑定值改变回调
            placeholder="请选择..."
        />          
    )
}
```
#### upload文件上传
```
constructor(props){
    super(props)
    this.uploadRef = React.createRef()
}
onChange(file){
    console.log(file)
}
render(){
    return(
        <Form.upload
            ref={ this.uploadRef }
            onChange={ this.onChange.bind(this) }
        >
            <Button type="primary" size="small">点击上传</Button>
        </Form.upload>       
    )
}
```
#### Form表单
在Form表单组件中，每一个表单域由一个 <Form.item></Form.item> 组件构成，表单域中可以放置各种类型的表单控件，包括 input、select、checkbox、radio、switch、calendar、timePicker、upload等
```
constructor(props){
    super(props)
    this.state = {
        value: ''
    }
}
onChange(value){
    console.log(value)
    this.setstate({
        value
    })
}
render(){
    return(
        <Form.form>
            <Form.item
                label="input："  //标签
                labelWidth="100px"  //标签宽度
            >
                <Form.input
                    type="text"
                    value={ this.state.value }
                    onChange={ this.onChange1.bind(this) }
                    placeholder="..."
                />
            </Form.item>
            <Form.item
                label="input："
                labelWidth="100px"
                full={ true }  //设置full属性为true，表单控件设置style={{ width: '100%' }}，表单控件宽度自动占满剩余宽度
            >
                <Form.select
                    type="text"
                    value={ this.state.value }
                    onChange={ this.onChange1.bind(this) }
                    placeholder="..."
                    style={{ width: '100%' }}
                />
                    <Form.option key={ index } label='label' value='1' />
                </Form.select>
            </Form.item>
        </Form.form>
    )
}
```
##### 表单验证
```
constructor(props){
    super(props)
    this.state = {
        inputValue: '',
        passwordValue: ''
    }
    this.rules = {  //验证对象
        text(value){  //方法名称对应Form.item中的prop属性
            if(value === ''){  //验证规则
                return '不能为空'  //验证不通过提示信息
            }
            return true  //验证通过需返回true
        },
        password(value){
            if(value === ''){
                return '不能为空'
            }else if(value.length < 6){
                return '长度需大于6'
            }
            return true
        }
    }
    this.rulesRef = React.createRef()
}
inputChange(value){
    this.setstate({
        inputValue: value
    })
}
passwordChange(value){
    this.setstate({
        passwordValue: value
    })
}
submit(){
    this.rulesRef.current.validate(valid => {  //调用表单验证判断方法
        if(valid){
            console.log('验证通过')
        }
    })
}
clearValidate(){
    this.rulesRef.current.clearValidate()  //清除表单验证提示
}
render(){
    return(
        <Form.form
            ref={ this.rulesRef }
            rules={ this.rules }  //添加表单验证规则rules属性
        >
            <Form.item
                label="input："
                labelWidth="100px"
                prop="text"  //添加prop属性，对应this.rules中的text
            >
                <Form.input
                    type="text"
                    value={ this.state.inputValue }
                    onChange={ this.inputChange.bind(this) }
                    placeholder="..."
                />
            </Form.item>
            <Form.item
                label="password："
                labelWidth="100px"
                prop="password"
            >
                <Form.input
                    type="password"
                    value={ this.state.passwordValue }
                    onChange={ this.passwordChange.bind(this) }
                    placeholder="..."
                />
            </Form.item>
            <Button type="primary" size="medium" onClick={ this.submit.bind(this) }>确定</Button>
            <Button type="primary" size="medium" onClick={ this.clearValidate.bind(this) }>清除表单验证</Button>
        </Form.form>
    )
}
```

### Tree树形展开
```
import { Tree } from '@/component/index'

constructor(props){
    super(props)
    this.state = {
        //checked：0: 选中，1：半选中，2: 未选中
        //只需给叶子节点设置checked属性选中状态，父级节点根据子节点状态更新自身选择状态
        //叶子节点checked(0: 选中，2: 未选中)，父级节点在checkboxChange回调中根据子节点状态，返回其自身的checked(0: 选中，1: 不定选，2: 未选中)
        data: [  
            {
                name: '一级-1',
                children: [
                    {
                        name: '二级-1',
                        children: [
                            {
                                name: '三级-1',
                                checked: 0
                            },
                            {
                                name: '三级-2',
                                checked: 2
                            }
                        ]
                    },
                    {
                        name: '二级-2',
                        checked: 2
                    }
                ]
            },
            {
                name: '一级-2',
                children: [
                    {
                        name: '二级-1',
                        checked: 0
                    },
                    {
                        name: '二级-2',
                        checked: 2
                    }
                ]
            },
            {
                name: '一级-3',
                checked: 2
            }
        ]
    }
    this.replaceProps = {
        label: 'name',
        children: 'children',
        checkedType: 'checked'
    }
}
getNode = node => {
    console.log(node)
}
checkboxChange = data => {
    console.log(data)
}
render(){
    return (
        <div>
            <Tree
                data={ this.state.data }  //树形展示数据
                replaceProps={ this.replaceProps }  //替换属性
                checkBox={ true }  //是否显示选择框
                nodeClick={ this.getNode }  //节点点击事件，返回当前点击节点项
                checkboxChange={ this.checkboxChange }  //选择框选择改变事件，返回整个树形展示数据状态
                render={  //节点插槽
                    item => {
                        return(
                            <div>...</div>
                        )
                    }
                }
            />
        </div>
    )
}
```

### Crop图片裁剪
```
import { Crop } from '@/component/index'
import img from '@/assets/images/crop.jpg'

constructor(props){
    super(props)
    this.state = {
        cropResult: '',
        img
    }
}
getCorpImg = blob => {
    let fileReader = new FileReader()
    fileReader.readAsDataURL(blob)
    fileReader.onload = e => {
        this.setState({
            cropResult: e.target.result
        })
    }
}
render(){
    return(
        <div>
            <Crop
                src={ this.state.img }  //可传入初始裁剪图片
                success={ this.getCorpImg }  //裁剪成功结果回调
            />
            {
                this.state.cropResult
                ?
                <div>
                    <h3>裁剪结果：</h3>
                    <img src={ this.state.cropResult } />
                </div>
                :
                null
            }
        </div>
    )
}
```

### Tooltip提示
```
import { Tooltip } from '@/component/index'

render(){
    return(
        <div>
            <Tooltip
                title="This is Tooltip"  //提示文字
                placement="bottom-left"  //提示出现位置，可选择值：'bottom-left'，'bottom-center'，'bottom-right'，'top-left'，    'top-center'，'top-right'，'left-top'，'left-center'，'left-bottom'，'right-top'，'right-center'，'right-bottom'
            >
                <span>bottom-left</span>
            </Tooltip>
            <Tooltip title="This is Tooltip">
                <span>bottom-center</span>
            </Tooltip>
            <Tooltip title="This is Tooltip" placement="bottom-right">
                <span>bottom-right</span>
            </Tooltip>
        </div>
    )
}
```

### Carousel跑马灯
```
import { Carousel } from '@/component/index'

render(){
    let arr = [1, 2, 3, 4, 5]
    return (
        <div>
            <Carousel.carousel
                duration={ 5000 }  //停顿时长
                autoplay={ true }  //是否自动切换
                currentChange={  //切换完成的回调，返回当前位置
                    current => {
                        console.log(current)
                    }
                }
            >
                <Carousel.item
                    render={  //设置渲染函数，应返回一个节点数组
                        () => {
                            return arr.map((item, index) => {
                                return (
                                    <div key={ index }>
                                        <span>{ item }</span>
                                    </div>
                                )
                            })
                        }
                    }
                />
                <Carousel.prevArrow
                    render={  //自定义上一页箭头展示
                        () => {
                            return (
                                <div>
                                    <span className="icon iconfont">&#xe630;</span>
                                </div>
                            )
                        }
                    }
                />
                <Carousel.nextArrow
                    render={  //自定义下一页箭头展示
                        () => {
                            return (
                                <div>
                                    <span className="icon iconfont">&#xe630;</span>
                                </div>
                            )
                        }
                    }
                />
                <Carousel.icon
                    render={  //自定义指示点分页展示，返回当前点击项的位置。若使用自定义指示点分页展示组件，外部可添加.carousel-icon-active样式类名，自定义添加当前指示点选中样式
                        index => {
                            return <span onClick={ () => { console.log(index) } }></span>
                        }
                    }
                />
                {/* <Carousel.prevArrow /> */}  //轮播组件内置一套箭头、指示点分页展示组件，可省略自定义的渲染函数
                {/* <Carousel.nextArrow /> */}
                {/* <Carousel.icon /> */}
            </Carousel.carousel>
        </div>
    )
}
```

### Progress进度条
```
import React, { useState } from "react"
import { Progress, Button } from '@/component/index'

export default function App(props) {
    const [percent, setPresent] = useState(50)
    return (
        <div>
            <Progress
                percent={ percent }  // 进度百分比
                type="circle"  // 进度条类型 circle: 圆形，line: 条形
                size={ 150 }  // type为circle圆形进度条时，可设置圆形进度条大小
                activeColor={  // 激活进度条颜色，若为数组，则分百分比段显示相关颜色。若为颜色字符串，则显示单一颜色。
                    [
                        {
                            color: '#c23a59',  // 0-20
                            percent: 0
                        },
                        {
                            color: '#753ac2',  // 20-40
                            percent: 20
                        },
                        {
                            color: '#3a70c2',  // 40-60
                            percent: 40
                        },
                        {
                            color: '#3ab5c2',  // 60-80
                            percent: 60
                        },
                        {
                            color: '#3ac270',  // 80-100
                            percent: 80
                        },
                        {
                            color: '#42c23a',  // 100
                            percent: 100
                        }
                    ]
                } 
                inActiveColor={ '#e5e9f2' }  // 进度条背景颜色
            >
                <div style={{ width: '100%', height: '100%', display: 'flex' }}>  // 圆形进度条中间区域自定义显示内容
                    <span style={{ margin: 'auto' }}>{ percent }%</span>
                </div>
            </Progress>
            <Progress
                percent={ percent }
                type="line"  // 条形进度条
                position="end"  // 渲染自定义内容位置，可选：end固定末尾，center固定中间，left：固定左侧，right当前进度条位置右侧，默认end
                strokeWidth={ 10 }  // 进度条宽度
                activeColor={ '#409eff' }
                inActiveColor={ '#e5e9f2' }
            >
                <span>{ percent }%</span>  // 自定义显示内容
            </Progress>
            <Progress
                percent={ percent }
                type="line"
                strokeWidth={ 10 }
                stripe={ true }  // 条形进度条斑马纹
                activeColor={ ['#008bdd', '#49beff'] }  // 若开启斑马纹，设置颜色数组交替显示斑马纹颜色
                inActiveColor={ '#e5e9f2' }
            />
            <div style={{ marginTop: '15px' }}>
                <Button onClick={ () => { setPresent(0) } }>0%</Button>
                <Button onClick={ () => { setPresent(10) } }>10%</Button>
                <Button onClick={ () => { setPresent(20) } }>20%</Button>
                <Button onClick={ () => { setPresent(30) } }>30%</Button>
                <Button onClick={ () => { setPresent(40) } }>40%</Button>
                <Button onClick={ () => { setPresent(50) } }>50%</Button>
                <Button onClick={ () => { setPresent(60) } }>60%</Button>
                <Button onClick={ () => { setPresent(70) } }>70%</Button>
                <Button onClick={ () => { setPresent(80) } }>80%</Button>
                <Button onClick={ () => { setPresent(90) } }>90%</Button>
                <Button onClick={ () => { setPresent(100) } }>100%</Button>
            </div>
        </div>
    )
}
```