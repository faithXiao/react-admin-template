import React, { useState } from "react"
import { Progress, Button } from '@/component/index'

export default function App(props) {
    const [percent, setPresent] = useState(50)
    return (
        <div style={{ padding: '15px' }}>
            <div style={{ display: 'flex' }}>
                <div style={{ width: '150px' }}>
                    <Progress percent={ percent } type="circle" size={ 150 } 
                        activeColor={
                            [
                                {
                                    color: '#c23a59',
                                    percent: 0
                                },
                                {
                                    color: '#753ac2',
                                    percent: 20
                                },
                                {
                                    color: '#3a70c2',
                                    percent: 40
                                },
                                {
                                    color: '#3ab5c2',
                                    percent: 60
                                },
                                {
                                    color: '#3ac270',
                                    percent: 80
                                },
                                {
                                    color: '#42c23a',
                                    percent: 100
                                }
                            ]
                        } 
                        inActiveColor={ '#e5e9f2' }>
                        <div style={{ width: '100%', height: '100%', display: 'flex' }}>
                            <span style={{ margin: 'auto' }}>{ percent }%</span>
                        </div>
                    </Progress>
                </div>
                <div style={{ width: '150px' }}>
                    <Progress percent={ percent } type="circle" size={ 150 } activeColor={ '#67c23a' } inActiveColor={ '#e5e9f2' }>
                        <div style={{ width: '100%', height: '100%', display: 'flex' }}>
                            <span style={{ margin: 'auto' }}>{ percent }%</span>
                        </div>
                    </Progress>
                </div>
            </div>
            <div style={{ width: '500px', marginTop: '15px' }}>
                <Progress percent={ percent } type="line" strokeWidth={ 10 } activeColor={ '#409eff' } inActiveColor={ '#e5e9f2' } />
            </div>
            <div style={{ width: '500px', marginTop: '15px' }}>
                <Progress percent={ percent } type="line" position="end" strokeWidth={ 10 } activeColor={ '#409eff' } inActiveColor={ '#e5e9f2' }>
                    <span>{ percent }%</span>
                </Progress>
            </div>
            <div style={{ width: '500px', marginTop: '15px' }}>
                <Progress percent={ percent } type="line" position="right" strokeWidth={ 25 } activeColor={ '#71ff2b' } inActiveColor={ '#e5e9f2' }>
                    <span>{ percent }%</span>
                </Progress>
            </div>
            <div style={{ width: '500px', marginTop: '15px' }}>
                <Progress
                    percent={ percent }
                    type="line"
                    position="center"
                    strokeWidth={ 25 }
                    activeColor={
                        [
                            {
                                color: '#c23a59',
                                percent: 0
                            },
                            {
                                color: '#753ac2',
                                percent: 20
                            },
                            {
                                color: '#3a70c2',
                                percent: 40
                            },
                            {
                                color: '#3ab5c2',
                                percent: 60
                            },
                            {
                                color: '#3ac270',
                                percent: 80
                            },
                            {
                                color: '#42c23a',
                                percent: 100
                            }
                        ]
                    }
                    inActiveColor={ '#e5e9f2' }
                >
                    <span>{ percent }%</span>
                </Progress>
            </div>
            <div style={{ width: '500px', marginTop: '15px' }}>
                <Progress percent={ percent } type="line" stripe={ true } strokeWidth={ 25 } activeColor={ ['#008bdd', '#49beff'] } inActiveColor={ '#e5e9f2' } />
            </div>
            <div style={{ marginTop: '15px' }}>
                <Button onClick={ () => { setPresent(0) } }>0%</Button>
                <Button onClick={ () => { setPresent(10) } }>10%</Button>
                <Button onClick={ () => { setPresent(20) } }>20%</Button>
                <Button onClick={ () => { setPresent(30) } }>30%</Button>
                <Button onClick={ () => { setPresent(40) } }>40%</Button>
                <Button onClick={ () => { setPresent(50) } }>50%</Button>
                <Button onClick={ () => { setPresent(60) } }>60%</Button>
                <Button onClick={ () => { setPresent(70) } }>70%</Button>
                <Button onClick={ () => { setPresent(80) } }>80%</Button>
                <Button onClick={ () => { setPresent(90) } }>90%</Button>
                <Button onClick={ () => { setPresent(100) } }>100%</Button>
            </div>
        </div>
    )
}