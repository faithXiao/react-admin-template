import React from 'react'
import { Tag } from '@/component/index'
import cssStyle from './index.module.css'

class TagComponent extends React.Component{
    render(){
        return(
            <div className={ cssStyle.container }>
                <Tag size="mini" className={ cssStyle.tag }>defaule</Tag>
                <Tag size="small" type="primary" className={ cssStyle.tag }>primary</Tag>
                <Tag size="small" type="success" className={ cssStyle.tag }>success</Tag>
                <Tag size="medium" type="info" className={ cssStyle.tag }>info</Tag>
                <Tag type="warning" className={ cssStyle.tag }>warning</Tag>
                <Tag type="error">error</Tag>
            </div>
        )
    }
}

export default TagComponent