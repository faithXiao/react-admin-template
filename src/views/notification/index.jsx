import React from "react"
import { Notification, Button } from "../../component"

export default function App() {
    const open = position => {
        const notification = Notification()
        notification.notify({
            title: '这是一段提示！',
            message: 'newsoul',
            position,
            footer: <Button type="primary" size="mini" onClick={ () => {
                console.log('aaa')
                notification.close()
            } }>确定</Button>
        })
    }
    return (
        <div style={{ padding: 15 }}>
            <Button type="primary" size='small' onClick={ () => { open('bottom-right') } }>bottom-right</Button>
            <Button type="primary" size='small' onClick={ () => { open('bottom-left') } }>bottom-left</Button>
            <Button type="primary" size='small' onClick={ () => { open('top-right') } }>top-right</Button>
            <Button type="primary" size='small' onClick={ () => { open('top-left') } }>top-left</Button>
        </div>
    )
}