import React from "react"
import { useParams } from "react-router-dom"
import cssStyle from './index.module.css'

export default function Params(props){
    console.log(props)
    let { user, id } = useParams()
    return (
        <div className={ cssStyle.container }>
            <h3>user---{ user }</h3>
            <h3>id---{ id }</h3>
        </div>
    )
}