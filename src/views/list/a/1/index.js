import React from 'react'

class AnotherA1 extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            value: '/another/a/1'
        }
    }
    UNSAFE_componentWillMount(){
        
    }
    componentDidMount(){
        
    }
    render(){
        return(
            <div>
                <p>{ this.state.value }</p>
            </div>
        )
    }
}

export default AnotherA1