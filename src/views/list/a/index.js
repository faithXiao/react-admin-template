import React from 'react'
import RouterView from '@/router/routerView.js'
import cssStyle from './index.module.css'

class AnotherA extends React.Component{
    render(){
        return(
            <div className={ cssStyle.container }>
                <RouterView {...this.props} />
            </div>
        )
    }
}

export default AnotherA