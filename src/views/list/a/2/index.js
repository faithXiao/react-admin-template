import React from 'react'

class AnotherA2 extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            value: '/another/a/2'
        }
    }
    UNSAFE_componentWillMount(){
        
    }
    componentDidMount(){
        
    }
    render(){
        return(
            <div>
                <p>{ this.state.value }</p>
            </div>
        )
    }
}

export default AnotherA2