import React from 'react'
import cssStyle from './index.module.css'

class AnotherB extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            value: '/another/b'
        }
    }
    render(){
        return(
            <div className={ cssStyle.container }>
                <p>{ this.state.value }</p>
            </div>
        )
    }
}

export default AnotherB