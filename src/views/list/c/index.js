import React from 'react'
import PropTypes from 'prop-types'
import { Button } from '@/component'
import cssStyle from './index.module.css'

function Params(props){
    function goto(user, id){
        props.history.push({
            pathname: `/list/params/${user}/${id}`,
            state: true
        })
    }
    return(
        <div className={ cssStyle.container }>
            <div style={{ marginBottom: '10px' }}>
                <Button type="success" size="small" onClick={ () => { goto('a', 1) } } style={{ marginRight: '10px' }}>跳转</Button>
                <span>user: a, id: 1</span>
            </div>
            <div style={{ marginBottom: '10px' }}>
                <Button type="success" size="small" onClick={ () => { goto('b', 2) } } style={{ marginRight: '10px' }}>跳转</Button>
                <span>user: b, id: 2</span>
            </div>
        </div>
    )
}

Params.propTypes = {
    history: PropTypes.object
}

export default Params