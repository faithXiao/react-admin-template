import React from 'react'
import PropTypes from 'prop-types'
import cssStyle from './index.module.css'
import { Tree } from '@/component'
import { connect } from 'react-redux'
import actions from '@/store/actions.js'

class App extends React.Component{
    constructor(props){
        super(props)
        this.replaceProps = {
            label: 'name',
            children: 'children',
            checkedType: 'checked'
        }
        this.state = {
            authRouter: [
                {
                    path: "/list",
                    name: '一级导航',
                    children: [
                        {
                            path: 'a',
                            name: '二级导航',
                            children: [
                                {
                                    checked: 0,
                                    path: '1',
                                    name: '三级页面-1'
                                },
                                {
                                    checked: 0,
                                    path: '2',
                                    name: '三级页面-2'
                                }
                            ]
                        },
                        {
                            checked: 0,
                            path: 'b',
                            name: '二级页面'
                        },
                        {
                            checked: 0,
                            path: 'c',
                            name: '路由传参'
                        },
                        {
                            checked: 0,
                            path: 'params/:user/:id',
                            name: '路由参数'
                        }
                    ]
                },
                {
                    path: "/dialog",
                    redirect: '/dialog/index',
                    name: 'Dialog',
                    children: [
                        {
                            checked: 0,
                            path: 'index',
                            name: 'Dialog'
                        }
                    ]
                },
                {
                    path: "/button",
                    redirect: '/button/index',
                    name: 'Button',
                    children: [
                        {
                            checked: 0,
                            path: 'index',
                            name: 'Button'
                        }
                    ]
                },
                {
                    path: "/tag",
                    redirect: '/tag/index',
                    name: 'Tag',
                    children: [
                        {
                            checked: 0,
                            path: 'index',
                            name: 'Tag'
                        }
                    ]
                },
                {
                    path: "/table",
                    redirect: '/table/index',
                    name: 'Table',
                    children: [
                        {
                            checked: 0,
                            path: 'index',
                            name: 'Table'
                        }
                    ]
                },
                {
                    path: "/pagination",
                    redirect: '/pagination/index',
                    name: 'Pagination',
                    children: [
                        {
                            checked: 0,
                            path: 'index',
                            name: 'Pagination'
                        }
                    ]
                },
                {
                    path: "/alert",
                    redirect: '/alert/index',
                    name: 'Alert',
                    children: [
                        {
                            checked: 0,
                            path: 'index',
                            name: 'Alert'
                        }
                    ]
                },
                {
                    path: "/form",
                    redirect: '/form/index',
                    name: 'Form',
                    children: [
                        {
                            checked: 0,
                            path: 'index',
                            name: 'Form'
                        }
                    ]
                },
                {
                    path: "/toask",
                    redirect: '/toask/index',
                    name: 'Toask',
                    children: [
                        {
                            checked: 0,
                            path: 'index',
                            name: 'Toask'
                        }
                    ]
                },
                {
                    path: "/loading",
                    redirect: '/loading/index',
                    name: 'Loading',
                    children: [
                        {
                            checked: 0,
                            path: 'index',
                            name: 'Loading'
                        }
                    ]
                },
                {
                    path: "/crop",
                    redirect: '/crop/index',
                    name: 'Crop',
                    children: [
                        {
                            checked: 0,
                            path: 'index',
                            name: 'Crop'
                        }
                    ]
                },
                {
                    path: "/tooltip",
                    redirect: '/tooltip/index',
                    name: 'Tooltip',
                    children: [
                        {
                            checked: 0,
                            path: 'index',
                            name: 'Tooltip'
                        }
                    ]
                },
                {
                    path: "/carousel",
                    redirect: '/carousel/index',
                    name: 'Carousel',
                    children: [
                        {
                            checked: 0,
                            path: 'index',
                            name: 'Carousel'
                        }
                    ]
                }
            ]
        }
    }
    updateTree = data => {
        this.setState({
            authRouter: data
        })
    }
    getNode = node => {
        console.log(node)
    }
    checkboxChange = data => {
        console.log(data)
    }
    render(){
        return (
            <div className={ cssStyle.container }>
                <div className={ cssStyle.tree }>
                    <Tree data={ this.state.authRouter } replaceProps={ this.replaceProps } checkBox={ true } nodeClick={ this.getNode } checkboxChange={ this.checkboxChange }
                        render={
                            item => {
                                return(
                                    <div>{ item.name }</div>
                                )
                            }
                        }
                    />
                </div>
            </div>
        )
    }
}

App.propTypes = {
    match: PropTypes.object,
    authRouter: PropTypes.array,
    updateAuthRouter: PropTypes.func,
    updateAccount: PropTypes.func
}

export default App