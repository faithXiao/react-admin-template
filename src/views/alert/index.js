import React from 'react'
import cssStyle from './index.module.css'
import { Button, Alert, Toask } from '@/component/index'

class AlertComponent extends React.Component{
    showAlert(title){
        let alert = Alert()
        alert.showAlert({
            title: title,
            message: title,
            confirm: () => {
                let toask = Toask()
                toask.showToask({
                    title: 'confirm',
                    type: 'success',
                    duration: 1500
                })
            },
            cancel: () => {
                let toask = Toask()
                toask.showToask({
                    title: 'cancel',
                    type: 'error',
                    duration: 1500
                })
            }
        })
    }
    render(){
        return(
            <div className={ cssStyle.container }>
                <Button type="primary" size="small" onClick={ (() => { this.showAlert('alert 1') }).bind(this) }>show alert</Button>
                <Button type="primary" size="small" onClick={ (() => { this.showAlert('alert 2') }).bind(this) }>show alert</Button>
            </div>
        )
    }
}

export default AlertComponent