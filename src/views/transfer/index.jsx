import React from 'react'
import { Transfer } from '@/component/index'
import cssStyle from './index.module.css'
import { useState } from 'react'
import { useRef } from 'react'
import { Button } from '../../component'

export default function App() {
    const transferRef = useRef(null)
    const [data, setData] = useState([
        {
            id: 0,
            label: '选项0',
            checked: true
        },
        {
            id: 1,
            label: '选项1',
            checked: false
        },
        {
            id: 2,
            label: '选项2',
            checked: true
        },
        {
            id: 3,
            label: '选项3',
            checked: true
        },
        {
            id: 4,
            label: '选项4',
            checked: true
        }
    ])
    const [value, setValue] = useState([
        {
            id: 0,
            label: '选项0',
            checked: true
        },
        {
            id: 1,
            label: '选项1',
            checked: false
        },
        {
            id: 2,
            label: '选项2',
            checked: true
        },
        {
            id: 3,
            label: '选项3',
            checked: true
        },
        {
            id: 4,
            label: '选项4',
            checked: true
        }
    ])
    const confirm = () => {
        console.log(transferRef.current.getSelection())
    }
    const onChange = val => {
        console.log(val)
    }
    return (
        <div className={ cssStyle.container }>
            <div style={{ width: 580 }}>
                <Transfer data={ data } value={ value } ref={ transferRef } repeat={ true } checkBox={ false } onChange={ onChange } />
            </div>
            <Button onClick={ confirm }>确定</Button>
        </div>
    )
}