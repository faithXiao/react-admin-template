import React from 'react'
import AppMain from './appMain'
import SliderBar from './slideBar'
import Nav from './nav'
import PropTypes from 'prop-types'
import './index.css'
class Layout extends React.Component{
    state = {}
    mainRef = React.createRef()
    render(){
        return (
            <div className="layout">
                <div className="left" id='left'>
                    <SliderBar {...this.props}  />
                </div>
                <div className="right">
                    <div className="nav">
                        <Nav {...this.props} />
                    </div>
                    <div ref={ this.mainRef } id="main" className="main">
                        <React.Suspense fallback={ <div></div> }>
                            <AppMain { ...this.props } />
                        </React.Suspense>
                    </div>
                </div>
            </div>
        )
    }
}

Layout.propTypes = {
    currentRoutesTable: PropTypes.array,
    routesTable: PropTypes.array
}

export default Layout