import React from 'react'
import RouterView from '@/router/routerView.js'

class AppMain extends React.PureComponent{
    render(){
        return <RouterView {...this.props} />
    }
}

export default AppMain