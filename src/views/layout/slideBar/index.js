import React, { useEffect, useState } from 'react'
import { NavLink } from 'react-router-dom'
import arrow from '@/assets/images/arrow.png'
import PropTypes from 'prop-types'
import { Tooltip } from '@/component/index'
import { routesTable } from '@/frontendAuth.js'
import eventBus from '@/util/eventBus'
import { connect } from 'react-redux'
import actions from '@/store/actions.js'
import SvgIcon from '@/util/svgIcon'

function RouterLinkMap(props){
    let li = []
    let paddingLeft = 15 + (props.paddingLeft ? props.paddingLeft : 0)
    let sytle = {
        paddingLeft: paddingLeft + 'px',
        paddingRight: '15px'
    }
    props.routesTable.forEach(item => {
        if(!item.hidden){
            let hasChildren = (item.children !== undefined && Array.isArray(item.children) && item.children.length)
            if(hasChildren && !item.redirect){
                let Component = (
                    <NavLink
                        to={ item.redirect || item.fullPath }
                        className="link"
                        activeClassName="hurray"
                        style={ sytle }
                        onClick={ e => { props.menuClick(true, e) } }
                        title={ item.name }
                    >
                        {
                            item.icon
                            ?
                            <SvgIcon name={ item.icon } />
                            :
                            null
                        }
                        <span>{ item.name }</span>
                        <img src={ arrow } className="arrow" alt="arrow" style={{ transform: 'rotate(0)' }} />
                    </NavLink>
                )
                li.push(
                    <li key={ item.fullPath }>
                        <Tooltip title={ item.name } placement="right-center" hide={ props.expand }>
                            { Component }
                        </Tooltip>
                        <div className={ "slider " + Math.random() } style={{ display: 'none' }}>
                            <RouterLinkMap { ...props } routesTable={ item.children } paddingLeft={ paddingLeft } />
                        </div>
                    </li>
                )
            }else{
                let Component = (
                    <NavLink
                        to={ item.redirect || item.fullPath }
                        activeClassName="hurray"
                        className="link"
                        style={ sytle }
                        onClick={
                            e => {
                                if(props.location.pathname === (item.redirect || item.fullPath)){
                                    e.preventDefault()
                                }
                                props.menuClick(false)
                            }
                        }
                    >
                        {
                            item.icon
                            ?
                            <SvgIcon name={ item.icon } />
                            :
                            null
                        }
                        { item.name }
                    </NavLink>
                )
                li.push(
                    <li key={ item.fullPath }>
                        {
                            props.expand
                            ?
                            Component
                            :
                            <Tooltip title={ item.name } placement="right-center">
                                { Component }
                            </Tooltip>
                        }
                    </li>
                )
            }
        }
    })
    return (
        <ul id="sliderBar" className="slider-bar-ul" style={{ width: props.expand ? '200px' : '48px' }}>
            { li }
        </ul>
    )
}

const SliderBar = props => {
    let timer = null
    const hideMenu = () => {
        let el = Array.from(document.getElementById('sliderBar').getElementsByClassName('slider'))
        el.forEach(item => {
            if (item.previousElementSibling) {
                item.previousElementSibling.children[item.previousElementSibling.children.length - 1].style.transform = 'rotate(0)'
            }
            let height = item.offsetHeight
            item.style.height = height + 'px'
            window.requestAnimationFrame(() => {
                setTimeout(() => {
                    item.style.height = 0
                    setTimeout(() => {
                        item.style = 'display: none;'
                    }, 300)
                })
            })
        })
    }
    useEffect(() => {
        if (!props.expand) {
            hideMenu()
        }
    }, [props.expand])
    const toggleDir = e => {
        e.stopPropagation()
        e.preventDefault()
        const currentTarget = e.currentTarget
        const nextElementSibling = currentTarget.nextElementSibling
        if(nextElementSibling){
            if(timer){
                return
            }
            let open = nextElementSibling.style.display === 'block' ? true : false
            if(open){
                currentTarget.children[currentTarget.children.length - 1].style.transform = 'rotate(0)'
                let height = nextElementSibling.offsetHeight
                nextElementSibling.style.height = height + 'px'
                timer = window.requestAnimationFrame(() => {
                    setTimeout(() => {
                        nextElementSibling.style.height = 0
                        setTimeout(() => {
                            nextElementSibling.style = 'display: none;'
                            timer = null
                        }, 300)
                    })
                })
            }else{
                currentTarget.children[currentTarget.children.length - 1].style.transform = 'rotate(180deg)'
                nextElementSibling.style.display = 'block'
                let height = nextElementSibling.offsetHeight
                nextElementSibling.style.height = 0
                timer = window.requestAnimationFrame(() => {
                    setTimeout(() => {
                        nextElementSibling.style.height = height + 'px'
                        setTimeout(() => {
                            nextElementSibling.style = 'display: block;'
                            timer = null
                        }, 300)
                    })
                })
            }
        }
    }
    const menuExpand = () => {
        if (!props.expand) {
            props.toggleExpand()
        }
    }
    const menuClick = (isDir, e) => {
        // menuExpand()  //点击一级菜单展开菜单栏
        if (isDir) {
            menuExpand()  //点击目录展开菜单栏
            toggleDir(e)
        }
    }
    return(
        <RouterLinkMap
            { ...props }
            menuClick= { menuClick }
            expand={ props.expand }
            routesTable={ routesTable }
        />
    )
}

SliderBar.propTypes = {
    routesTable: PropTypes.array,
    location: PropTypes.object,
    expand: PropTypes.bool
}

RouterLinkMap.propTypes = {
    routesTable: PropTypes.array,
    location: PropTypes.object,
    paddingLeft: PropTypes.number,
    menuClick: PropTypes.func,
    expand: PropTypes.bool,
}

function mapStateToProps(state) {
    return {
        expand: state.expand
    }
}
function mapDispatchToProps(dispatch) {
    return {
        toggleExpand: () => dispatch(actions.toggleExpand())
    }
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SliderBar)
