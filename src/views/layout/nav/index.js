/* eslint-disable react/prop-types */
import React from 'react'
import cssStyle from './index.module.css'
import { Button, Alert, Toask, Tooltip } from '@/component/index'
import actions from '@/store/actions.js'
import { connect } from 'react-redux'
import eventBus from '@/util/eventBus'

function getPageList(route, pathname, pageList = []){
    if(route.fullPath === pathname){
        pageList = [route.name]
    }else{
        let hasChildren = (route.children !== undefined && Array.isArray(route.children) && route.children.length)
        if(hasChildren){  //若存在子路由页面
            for(let i = 0; i < route.children.length; i++){
                if(route.children[i].fullPath === pathname){  //当前路径匹配路由一致，则直接获取所有页面导航
                    route.children[i].parent.forEach(item => {
                        pageList.push(item.name)
                    })
                    pageList.push(route.children[i].name)
                    break
                }else{
                    let pathArr = route.children[i].fullPath.split('/')
                    let isParamsPath = pathArr.filter(item => {
                        return ((item.indexOf(':') === 0) && (item.replace(/:/g, '') !== ''))
                    }).length > 0
                    if(isParamsPath){  //判断是否为携带参数的路由
                        let isMacth = true  //路径参数匹配标志
                        let pathnameArr = pathname.split('/')
                        for(let i = 0; i < pathArr.length; i++){
                            let isParams = pathArr[i].indexOf(':') === 0 && pathArr[i].replace(/:/g, '') !== ''
                            if(isParams){
                                continue
                            }else{
                                if(pathArr[i] !== pathnameArr[i]){
                                    isMacth = false
                                }
                            }
                        }
                        if(isMacth){  //当前路径与路由参数匹配一致，则直接获取所有页面导航
                            route.children[i].parent.forEach(item => {
                                pageList.push(item.name)
                            })
                            pageList.push(route.children[i].name)
                            break
                        }else{  //当前路径与路由参数匹配不一致，再递归遍历子路由
                            getPageList(route.children[i], pathname, pageList)
                        }
                    }else{  //当前路径匹配路由不一致，也不是携带参数的路由，再递归遍历子路由
                        getPageList(route.children[i], pathname, pageList)
                    }
                }
            }
        }else{  //不存在子路由，即属于一级页面，直接获取当前页面导航
            pageList = [route.name]
        }
        if(pageList.length === 2 && pageList[0] === pageList[1]){
            pageList = [pageList[0]]
        }
    }
    return pageList
}

class Nav extends React.PureComponent{
    iconRef = React.createRef()
    logout = () => {
        let alert = Alert()
        alert.showAlert({
            title: '提示',
            message: '是否退出登录？',
            confirm: async () => {
                try {
                    this.props.logout()
                    window.sessionStorage.removeItem('token')
                    eventBus.emit('updateRoutesTable')
                    let toask = Toask()
                    toask.showToask({
                        title: '退出成功',
                        type: 'success',
                        duration: 2000
                    })
                } catch (error) {
                    console.log(error)
                }
            },
            cancel: () => {

            }
        })
    }
    expandToggle = () => {
        this.props.toggleExpand()
    }
    render(){
        let pageList = getPageList(this.props.route, this.props.location.pathname)
        pageList = pageList.filter(item => {
            return item
        })
        return(
            <div className={ cssStyle.navBar }>
                <Tooltip title={ this.props.expand ? '收起' : '展开' } placement="bottom-center">
                    <svg className={ cssStyle.icon } ref={ this.iconRef } onClick={ this.expandToggle } style={{ transform: this.props.expand ? 'rotate(0deg)' : 'rotate(90deg)' }}>
                        <use xlinkHref="#icon-expand"></use>
                    </svg>
                </Tooltip>
                <div className={ cssStyle.navBarLink }>
                    {
                        pageList.map((item, index) => {
                            return(
                                <span key={ index }>
                                    { item }
                                    {
                                        index !== pageList.length - 1
                                        ?
                                        <span className={ cssStyle.cut }>/</span>
                                        :
                                        null
                                    }
                                </span>
                            )
                        })
                    }
                </div>
                <div className={ cssStyle.right }>
                    <img src={ this.props.loginInfo.avatar } />
                    <span>{ this.props.loginInfo.name }</span>
                    <Button type="error" size="small" onClick={ this.logout }>退出</Button>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        loginInfo: state.loginInfo,
        expand: state.expand
    }
}
function mapDispatchToProps(dispatch) {
    return {
        logout: () => dispatch(actions.logout()),
        toggleExpand: () => dispatch(actions.toggleExpand())
    }
}
let NavComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)(Nav)

export default React.memo(NavComponent, (prevProps, nextProps) => {
    return prevProps.location.pathname === nextProps.location.pathname && prevProps.expand === nextProps.expand
})