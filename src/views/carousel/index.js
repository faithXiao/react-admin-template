import React from 'react'
import { Carousel } from '@/component/index'
import './index.css'

export default class App extends React.Component{
    render(){
        let arr = [1, 2, 3, 4, 5]
        return (
            <div className="carousel-container">
                <Carousel.carousel
                    duration={ 5000 }
                    autoplay={ true }
                    currentChange={
                        current => {
                            console.log(current)
                        }
                    }
                >
                    <Carousel.item
                        render={
                            () => {
                                return arr.map((item, index) => {
                                    return (
                                        <div key={ index } className="carousel-item">
                                            <span>{ item }</span>
                                        </div>
                                    )
                                })
                            }
                        }
                    />
                    {/* <Carousel.prevArrow
                        render={
                            () => {
                                return (
                                    <div style={{ transform: 'rotate(90deg)' }}>
                                        <span className="icon iconfont">&#xe630;</span>
                                    </div>
                                )
                            }
                        }
                    />
                    <Carousel.nextArrow
                        render={
                            () => {
                                return (
                                    <div style={{ transform: 'rotate(-90deg)' }}>
                                        <span className="icon iconfont">&#xe630;</span>
                                    </div>
                                )
                            }
                        }
                    /> */}
                    <Carousel.prevArrow />
                    <Carousel.nextArrow />
                    <Carousel.icon />
                    {/* <Carousel.icon
                        render={
                            index => {
                                return <span className="carousel-icon" onClick={ () => { console.log(index) } }></span>
                            }
                        }
                    /> */}
                </Carousel.carousel>
            </div>
        )
    }
}