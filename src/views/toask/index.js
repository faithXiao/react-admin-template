import React from 'react'
import { Button, Toask } from '@/component/index'
import cssStyle from './index.module.css'

export default class LoadingComponent extends React.Component{
    showToask(duration, type){
        let toask = Toask()
        toask.showToask({
            type,
            title: type,
            duration
        })
    }
    render(){
        return (
            <div className={ cssStyle.container }>
                <Button size="small" type="primary" style={{ marginRight: '15px' }} onClick={ (() => { this.showToask(1500, 'info') }).bind(this) }>show Toask 1.5s</Button>
                <Button size="small" type="success" style={{ marginRight: '15px' }} onClick={ (() => { this.showToask(3000, 'success') }).bind(this) }>show Toask 3s</Button>
                <Button size="small" type="error" style={{ marginRight: '15px' }} onClick={ (() => { this.showToask(3000, 'error') }).bind(this) }>show Toask 3s</Button>
                <Button size="small" type="default" style={{ marginRight: '15px' }} onClick={ (() => { this.showToask(0, 'warning') }).bind(this) }>show Toask noClose</Button>
            </div>
        )
    }
}