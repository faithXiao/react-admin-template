import React from 'react'
import { Button, Table, Alert, Toask, Pagination, Dialog } from '@/component/index'
import cssStyle from './index.module.css'

class TabelComponent extends React.Component{
    constructor(props){
        super(props)
        let data = new Array(100).fill({})
        // eslint-disable-next-line no-unused-vars
        data = data.map((item, index) => {
            return {
                id: index + 1,
                name: '王五',
                sex: '男',
                age: 22,
                address: '北京'
            }
        })
        this.tableData = data
        this.state = {
            data: [],
            page: 1,
            size: 15,
            count: 7,
            showDialog: false,
            info: {},
            loading: true
        }
    }
    editItem(item){
        this.setState({
            info: {...item},
            showDialog: true
        })
    }
    closeDialog(){
        this.setState({
            info: {},
            showDialog: false
        })
    }
    deleteItem(item, index){
        let alert = Alert()
        alert.showAlert({
            title: '提示',
            message: '确认删除吗？',
            confirm: () => {
                this.setState(state => {
                    state.data.splice((index + (this.state.page - 1)*this.state.size), 1)
                    return{
                        data: state.data
                    }
                }, () => {
                    let toask = Toask()
                    toask.showToask({
                        title: '删除成功',
                        type: 'success',
                        duration: 1500
                    })
                })
            },
            cancel: () => {

            }
        })
    }
    currentChange(e){
        this.setState({
            page: e,
            loading: true
        }, () => {
            setTimeout(() => {
                let data = [...this.tableData]
                let page = this.state.page
                let size = this.state.size
                let start = (page - 1)*size
                let end = start + size > data.length ? data.length : start + size
                data = data.slice(start, end)
                this.setState({
                    data,
                    loading: false
                })
            }, 1500)
        })
    }
    multipleSelect(value){
        console.log(value)
    }
    componentDidMount(){
        setTimeout(() => {
            let data = [...this.tableData]
            let page = this.state.page
            let size = this.state.size
            let start = (page - 1)*size
            let end = start + size > data.length ? data.length : start + size
            data = data.slice(start, end)
            this.setState({
                data,
                loading: false
            })
        }, 1500)
    }
    componentWillUnmount = () => {
        this.setState = () => {
            return
        }
    }
    render(){
        return(
            <div className={ cssStyle.container }>
                <Table.table
                    data={ this.state.data }
                    align="center"
                    rowKey="id"
                    selectionChange={ (value => { this.multipleSelect(value) }).bind(this) }
                    loading={ this.state.loading }
                    stripe={ true }
                >
                    <Table.column type="expand" width="60"
                        lazy={ true }
                        render={
                            item => {
                                return new Promise(resolve => {
                                    setTimeout(() => {
                                        resolve(
                                            <div style={{ textAlign: 'left' }}>
                                                <p>id: { item.id }</p>
                                                <p>name: { item.name }</p>
                                                <p>sex: { item.sex }</p>
                                                <p>age: { item.age }</p>
                                                <p>address: { item.address }</p>
                                            </div>
                                        )
                                    }, 1000)
                                })
                            }
                        }
                    />
                    <Table.column type="selection" width="60" />
                    <Table.column prop="id" label="id" width="80" />
                    <Table.column prop="name" label="姓名" width="120" />
                    <Table.column prop="sex" label="性别" width="120" />
                    <Table.column prop="age" label="年龄" width="120" />
                    <Table.column prop="address" label="地址" width="150" />
                    <Table.column label="操作" width="180"
                        render={
                            (item, index) => (
                                <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                                    <Button type="success" size="small" style={{ marginRight: '10px', flex: 'none' }} onClick={ (() => { this.editItem(item, index) }).bind(this) }>查看</Button>
                                    <Button type="error" size="small" style={{ flex: 'none' }} onClick={ (() => { this.deleteItem(item, index) }).bind(this) }>删除</Button>
                                </div>
                            )
                        }
                    />
                </Table.table>
                <div style={{ textAlign: 'right', marginTop: '15px' }}>
                    <Pagination
                        total={ this.tableData.length }
                        size={ this.state.size }
                        count={ this.state.count }
                        current={ this.state.page }
                        currentChange={ this.currentChange.bind(this) }
                    />
                </div>
                
                <Dialog
                    title="edit"
                    show={ this.state.showDialog }
                    onCancel={ this.closeDialog.bind(this) }
                    width="30%"
                >
                    <div>
                        <span>id：</span>
                        <span>{ this.state.info.id }</span>
                    </div>
                    <div>
                        <span>name：</span>
                        <span>{ this.state.info.name }</span>
                    </div>
                    <div>
                        <span>sex：</span>
                        <span>{ this.state.info.sex }</span>
                    </div>
                    <div>
                        <span>address：</span>
                        <span>{ this.state.info.address }</span>
                    </div>
                    <div style={{ textAlign: 'right', marginTop: '15px' }}>
                        <Button type="primary" size="medium" onClick={ this.closeDialog.bind(this) }>确定</Button>
                    </div>
                </Dialog>
            </div>
        )
    }
}

export default TabelComponent