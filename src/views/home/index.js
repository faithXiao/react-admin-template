import React from 'react'
import cssStyle from './index.module.css'
import data from '../../assets/document/index.html?url'

function App() {
    return (
        <div className={ cssStyle.app }>
            <iframe src={ data } frameBorder={ 0 } style={{ width: '100%', height: 'calc(100vh - 50px)', display: 'block' }}></iframe>
        </div>
    );
}

export default App
