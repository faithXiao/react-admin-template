import React from 'react'
import { Button } from '@/component/index'
import cssStyle from './index.module.css'

export default function ButtonComponent(props){
    function goto(user, id){
        props.history.push({
            pathname: `/list/params/${user}/${id}`,
            state: true
        })
    }
    return (
        <div className={ cssStyle.container }>
            <Button size="mini" className={ cssStyle.btn } onClick={ () => { goto('user', 1) } }>defaule</Button>
            <Button size="small" type="primary" className={ cssStyle.btn }>primary</Button>
            <Button size="medium" type="success" className={ cssStyle.btn }>success</Button>
            <Button type="info" className={ cssStyle.btn }>info</Button>
            <Button type="warning" className={ cssStyle.btn }>warning</Button>
            <Button type="error" className={ cssStyle.btn }>error</Button>

            <Button type="primary" pain className={ cssStyle.btn }>primary pain</Button>
            <Button type="success" pain className={ cssStyle.btn }>success pain</Button>
            <Button type="info" pain className={ cssStyle.btn }>info pain</Button>
            <Button type="warning" pain className={ cssStyle.btn }>warning pain</Button>
            <Button type="error" pain>error pain</Button>
        </div>
    )
}