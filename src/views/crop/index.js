import React from 'react'
import { Crop } from '@/component/index'
import cssStyle from './index.module.css'
import img from '@/assets/images/crop.jpg'

class App extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            cropResult: '',
            img
        }
    }
    getCorpImg = blob => {
        let fileReader = new FileReader()
        fileReader.readAsDataURL(blob)
        fileReader.onload = e => {
            this.setState({
                cropResult: e.target.result
            })
        }
    }
    render(){
        return(
            <div className={ cssStyle.container }>
                <Crop src={ this.state.img } success={ this.getCorpImg } size={ 600 } />
                {
                    this.state.cropResult
                    ?
                    <div>
                        <h3>裁剪结果：</h3>
                        <img src={ this.state.cropResult } />
                    </div>
                    :
                    null
                }
            </div>
        )
    }
}

export default App