import React, { useState, useEffect } from "react"
import socket from '@/websocket'
import eventBus from '@/util/eventBus'
import { Button, Form, Toask } from "@/component/index"

export default function App(props) {
    const [msg, setMsg] = useState('')
    const sendMsg = () => {
        socket.emit('msg', msg, (err, data) => {
            if (err) {
                console.log(err)
                return
            }
            setMsg('')
        })
    }
    useEffect(() => {
        eventBus.on('msg', data => {
            console.log(data)
        })
    }, [])
    return (
        <div>
            <Form.input type="textarea" value={ msg } onChange={ val => { setMsg(val) } } />
            <div>
                <Button type="primary" size="small" onClick={ sendMsg }>发送</Button>
            </div>
        </div>
    )
}