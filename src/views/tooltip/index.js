import React from 'react'
import { Tooltip } from '@/component/index'
import cssStyle from './index.module.css'

class App extends React.Component{
    render(){
        return(
            <div className={ cssStyle.container }>
                <Tooltip title="bottom-left" placement="bottom-left">
                    <span>bottom-left</span>
                </Tooltip>
                <Tooltip title="bottom-center">
                    <span>bottom-center</span>
                </Tooltip>
                <Tooltip title="bottom-right" placement="bottom-right">
                    <span>bottom-right</span>
                </Tooltip>
                <br />
                <br />
                <br />
                <Tooltip title="top-left" placement="top-left">
                    <span>top-left</span>
                </Tooltip>
                <Tooltip title="top-center" placement="top-center">
                    <span>top-center</span>
                </Tooltip>
                <Tooltip title="top-right" placement="top-right">
                    <span>top-right</span>
                </Tooltip>
                <br />
                <br />
                <br />
                <Tooltip title="left-top" placement="left-top">
                    <span>left-top</span>
                </Tooltip>
                <Tooltip title="left-center" placement="left-center">
                    <span>left-center</span>
                </Tooltip>
                <Tooltip title="left-bottom" placement="left-bottom">
                    <span>left-bottom</span>
                </Tooltip>
                <br />
                <br />
                <br />
                <Tooltip title="right-top" placement="right-top">
                    <span>right-top</span>
                </Tooltip>
                <Tooltip title="right-center" placement="right-center">
                    <span>right-center</span>
                </Tooltip>
                <Tooltip title="right-bottom" placement="right-bottom">
                    <span>right-bottom</span>
                </Tooltip>
            </div>
        )
    }
}

export default App