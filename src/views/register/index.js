import React from 'react'
import { Button, Loading, Toask, Form } from '@/component/index'
import cssStyle from './index.module.css'
import PropTypes from 'prop-types'
import { register } from '@/api/user'

class Register extends React.Component{
    constructor(props){
        super(props)
        this.rules = {
            account(value){
                if(value === ''){
                    return '账号不能为空'
                }
                return true
            },
            name(value){
                if(value === ''){
                    return '名称不能为空'
                }
                return true
            },
            password(value){
                if(value === ''){
                    return '密码不能为空'
                }else if(value.length < 5){
                    return '长度需大于5'
                }
                return true
            }
        }
        this.rulesRef = React.createRef()
        this.state = {
            form: {
                account: '',
                password: '',
                name: ''
            }
        }
    }
    regiser(){
        this.rulesRef.current.validate(valid => {
            if(valid){
                let loading = Loading()
                loading.showLoading('注册中...')
                register(this.state.form).then(res => {
                    if(res.status){
                        let toask = Toask()
                        toask.showToask({
                            title: '注册成功',
                            duration: 1500,
                            type: 'success'
                        })
                        this.props.history.push('/login')
                    }
                }).catch(e => {
                    console.log(e)
                }).finally(() => {
                    loading.hideLoading()
                })
            }
        })
    }
    onChange = type => {
        return value => {
            this.setState(state => {
                state.form[type]= value
                return state
            })
        }
        
    }
    render(){
        return (
            <div className={ cssStyle.container }>
                <Form.form className={ cssStyle.form + ' ' + cssStyle.btn } ref={ this.rulesRef } form={ this.state.form } rules={ this.rules }>
                    <div>
                        <Form.item label="账号：" labelWidth="80px" prop="account">
                            <Form.input type="text" className={ cssStyle.input } value={ this.state.form.account } onChange={ this.onChange('account') } placeholder="请输入账号..." />
                        </Form.item>
                    </div>
                    <div>
                        <Form.item label="名称：" labelWidth="80px" prop="name">
                            <Form.input type="text" className={ cssStyle.input } value={ this.state.form.name } onChange={ this.onChange('name') } placeholder="请输入名称..." />
                        </Form.item>
                    </div>
                    <div>
                        <Form.item label="密码：" labelWidth="80px" prop="password">
                            <Form.input type="password" className={ cssStyle.input } value={ this.state.form.password } onChange={ this.onChange('password') } placeholder="请输入密码..." />
                        </Form.item>
                    </div>
                    <Button onClick={ this.regiser.bind(this) } type="primary" style={{ marginLeft: '80px' }}>register</Button>
                    <Button onClick={ () => { this.props.history.push('/login') } } type="primary" style={{ marginLeft: '12px' }}>login</Button>
                </Form.form>
            </div>
        )
    }
}

Register.propTypes = {
    history: PropTypes.object,
    login: PropTypes.func,
    account: PropTypes.string,
    password: PropTypes.string,
    updateAccount: PropTypes.func,
    updatePassword: PropTypes.func
}

export default Register