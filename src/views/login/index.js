import React from 'react'
import { connect } from 'react-redux'
import { Button, Loading, Toask, Form } from '@/component/index'
import cssStyle from './index.module.css'
import PropTypes from 'prop-types'
import actions from '@/store/actions.js'
import eventBus from '@/util/eventBus'

class Login extends React.Component{
    constructor(props){
        super(props)
        this.rules = {
            account(value){
                if(value === ''){
                    return '账号不能为空'
                }
                return true
            },
            password(value){
                if(value === ''){
                    return '密码不能为空'
                }else if(value.length < 5){
                    return '长度需大于5'
                }
                return true
            }
        }
        this.rulesRef = React.createRef()
        this.state = {
            form: {
                account: props.account,
                password: props.password
            }
        }
    }
    login(){
        this.rulesRef.current.validate(valid => {
            if(valid){
                let loading = Loading()
                loading.showLoading('登录中...')
                this.props.login({
                    account: this.props.account,
                    password: this.props.password
                }).then(() => {
                    let toask = Toask()
                    toask.showToask({
                        title: '登陆成功',
                        duration: 1500,
                        type: 'success'
                    })
                    window.sessionStorage.setItem('token', '123456')
                    eventBus.emit('updateRoutesTable')
                    this.props.history.replace('/')
                }).catch(e => {
                    console.log(e)
                }).finally(() => {
                    loading.hideLoading()
                })
            }
        })
    }
    onChange1(value){
        this.setState(state => {
            state.form.account = value
            return state
        })
        this.props.updateAccount(value)
    }
    onChange2(value){
        this.setState(state => {
            state.form.password = value
            return state
        })
        this.props.updatePassword(value)
    }
    render(){
        return (
            <div className={ cssStyle.container }>
                <Form.form className={ cssStyle.form + ' ' + cssStyle.btn } ref={ this.rulesRef } form={ this.state.form } rules={ this.rules }>
                    <div>
                        <Form.item label="账号：" labelWidth="80px" prop="account">
                            <Form.input type="text" className={ cssStyle.input } value={ this.state.form.account } onChange={ this.onChange1.bind(this) } placeholder="请输入账号..." />
                        </Form.item>
                    </div>
                    <Form.item label="密码：" labelWidth="80px" prop="password">
                        <Form.input type="password" className={ cssStyle.input } value={ this.state.form.password } onChange={ this.onChange2.bind(this) } placeholder="请输入密码..." />
                    </Form.item>
                    <Button onClick={ this.login.bind(this) } type="primary" style={{ marginLeft: '80px' }}>登录</Button>
                </Form.form>
            </div>
        )
    }
}

Login.propTypes = {
    history: PropTypes.object,
    login: PropTypes.func,
    account: PropTypes.string,
    password: PropTypes.string,
    updateAccount: PropTypes.func,
    updatePassword: PropTypes.func
}

function mapStateToProps(state) {
    return {
        account: state.account,
        password: state.password
    }
}
function mapDispatchToProps(dispatch) {
    return {
        updateAccount: val => dispatch(actions.updateAccount(val)),
        updatePassword: val => dispatch(actions.updatePassword(val)),
        login: (account, password) => dispatch(actions.login(account, password))
    }
}
let LoginComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)(Login)

export default LoginComponent