import React from 'react'
import { Pagination } from '@/component/index'
import cssStyle from './index.module.css'

export default class PaginationComponent extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            total: 120,
            page: 1,
            size: 12,
            count: 7
        }
    }
    currentChange(e){
        this.setState({
            page: e
        })
    }
    render(){
        return(
            <div className={ cssStyle.container }>
                <Pagination
                    total={ this.state.total }
                    size={ this.state.size }
                    count={ this.state.count }
                    current={ this.state.page }
                    currentChange={ this.currentChange.bind(this) }
                />
            </div>
        )
    }
}