import React from 'react'
import { Button, Loading } from '@/component/index'
import cssStyle from './index.module.css'

export default class LoadingComponent extends React.Component{
    showLoading(duration){
        let loading = Loading()
        loading.showLoading('加载中...')
        setTimeout(() => {
            loading.hideLoading()
        }, duration)
    }
    render(){
        return (
            <div className={ cssStyle.container }>
                <Button size="small" type="primary" style={{ marginRight: '15px' }} onClick={ (() => { this.showLoading(1500) }).bind(this) }>show Loading 1.5s</Button>
                <Button size="small" type="success" style={{ marginRight: '15px' }} onClick={ (() => { this.showLoading(3000) }).bind(this) }>show Loading 3s</Button>
                <Button size="small" type="default" style={{ marginRight: '15px' }} onClick={ (() => { this.showLoading(4500) }).bind(this) }>show Loading 4.5s</Button>
            </div>
        )
    }
}
