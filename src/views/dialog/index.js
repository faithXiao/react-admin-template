import React from 'react'
import { Dialog, Button, Form } from '@/component/index'
import cssStyle from './index.module.css'

class DialogPage extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            show1: false,
            show2: false,
            select: '',
            time: '',
            value: ''
        }
    }
    showDialog1(){
        this.setState({
            show1: true
        })
    }
    beforeClose1(){
        console.log('beforeClose1')
    }
    confirm1(){
        this.setState({
            show1: false
        })
    }
    showDialog2(){
        this.setState({
            show2: true
        })
    }
    beforeClose2(){
        console.log('beforeClose2')
    }
    confirm2(){
        this.setState({
            show2: false
        })
    }
    setFormData = prop => {
        return value => {
            this.setState({
                [prop]: value
            })
        }
    }
    render(){
        return (
            <div className={ cssStyle.container }>
                <Button
                    type="primary"
                    className={ cssStyle.btn }
                    style={{ marginRight: '15px' }}
                    onClick={ this.showDialog1.bind(this) }
                >
                    show Dialog 1
                </Button>
                <Button onClick={ this.showDialog2.bind(this) } type="primary" className={ cssStyle.btn }>show Dialog 2</Button>
                <Dialog
                    show={ this.state.show1 }
                    title="Dialog 1"
                    width="600px"
                    onCancel={ () => this.setState({ show1: false }) }
                    beforeClose={ this.beforeClose1.bind(this) }
                >
                    <Form.form>
                        <Form.item label="select：" labelWidth="100px" prop="select">
                            <Form.select value={ this.state.select } onChange={ this.setFormData('select') } placeholder="请选择...">
                                <Form.option label="option 1" value="1" />
                                <Form.option label="option 2" value="2" />
                                <Form.option label="option 3" value="3" />
                                <Form.option label="option 4" value="4" />
                                <Form.option label="option 5" value="5" />
                                <Form.option label="option 6" value="6" />
                                <Form.option label="option 7" value="7" />
                                <Form.option label="option 8" value="8" />
                                <Form.option label="option 9" value="9" />
                                <Form.option label="option 10" value="10" />
                                <Form.option label="option 11" value="11" />
                                <Form.option label="option 12" value="12" />
                                <Form.option label="option 13" value="13" />
                                <Form.option label="option 14" value="14" />
                            </Form.select>
                        </Form.item>
                        <Form.item label="input：" labelWidth="100px">
                            <Form.calendar type="text" value={ this.state.value } splitCode="-" onChange={ this.setFormData('value') } placeholder="请选择..." />
                        </Form.item>
                        <div className={ cssStyle.footer }>
                            <Button onClick={ () => this.setState({ show1: false }) } type="default" style={{ marginRight: '10px' }}>取消</Button>
                            <Button onClick={ this.confirm1.bind(this) } type="success">确定</Button>
                        </div>
                    </Form.form>
                </Dialog>
                <Dialog
                    show={ this.state.show2 }
                    title="Dialog 2"
                    width="600px"
                    onCancel={ () => this.setState({ show2: false }) }
                    beforeClose={ this.beforeClose2.bind(this) }
                >
                    <Form.form>
                        <Form.item label="input：" labelWidth="100px">
                            <Form.input type="text" value={ this.state.value } onChange={ this.setFormData('value') } />
                        </Form.item>
                    </Form.form>
                    <div className={ cssStyle.footer }>
                        <Button onClick={ () => this.setState({ show2: false }) } type="default" style={{ marginRight: '10px' }}>取消</Button>
                        <Button onClick={ this.confirm2.bind(this) } type="success">确定</Button>
                    </div>
                </Dialog>
            </div>
        )
    }
}

export default DialogPage