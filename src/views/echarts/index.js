import React, { useEffect } from 'react'
import * as echarts from 'echarts'
import cssStyle from './index.module.css'
import eventBus from '@/util/eventBus'

let myChart1 = null, myChart2 = null
let timer = null
const option1 = {
    xAxis: {
        type: 'category'
    },
    yAxis: {},
    tooltip: {
        trigger: 'axis',
        formatter: params => {
            let res = params[0].name + '<br />'
            params.forEach((item, index) => {
                res += item.marker + ' ' + item.seriesName + ': ' + item.data[index + 1] + '<br />'
            })
            return res
        }
    },
    legend: {
        top: 20
    },
    dataset: {
        dimensions: ['week', 'bar', 'line'],
        source: [
            ['Mon', 50, 50],
            ['Tue', 20, 20],
            ['Wed', 30, 30],
            ['Thu', 60, 60],
            ['Fri', 40, 40],
            ['Sat', 10, 10],
            ['Sun', 70, 70]
        ]
    },
    series: [
        { type: 'bar' },
        { type: 'line', smooth: true }
    ]
}
const option2 = {
    dataset: {
        source: [
            ['Mon', 50],
            ['Tue', 20],
            ['Wed', 30],
            ['Thu', 60],
            ['Fri', 40],
            ['Sat', 10],
            ['Sun', 70]
        ]
    },
    series: [
        { type: 'pie', roseType: 'area' }
    ]
}
export default function App() {
    function resize() {
        if (timer) {
            window.cancelAnimationFrame(timer)
        }
        timer = window.requestAnimationFrame(() => {
            myChart1.resize()
            myChart2.resize()
        })
    }
    function animation() {
        setTimeout(() => {
            const source1 = [
                ['Mon', 0, 0],
                ['Tue', 0, 0],
                ['Wed', 0, 0],
                ['Thu', 0, 0],
                ['Fri', 0, 0],
                ['Sat', 0, 0],
                ['Sun', 0, 0]
            ]
            const source2 = [
                ['Mon', 0],
                ['Tue', 0],
                ['Wed', 0],
                ['Thu', 0],
                ['Fri', 0],
                ['Sat', 0],
                ['Sun', 0]
            ]
            for (let i = 0; i < source1.length; i += 1) {
                const num = parseFloat((Math.random()*100).toFixed(2))
                source1[i][1] = source1[i][2] = num
                source2[i][1] = num
            }
            option1.dataset.source = source1
            option2.dataset.source = source2
            myChart1.setOption(option1)
            myChart2.setOption(option2)
            animation()
        }, 3000)
    }
    function menuExpand() {
        setTimeout(() => {
            resize()
        }, 300)
    }
    useEffect(() => {
        myChart1 = echarts.init(document.getElementById('echarts-1'))
        myChart1.setOption(option1)
        myChart2 = echarts.init(document.getElementById('echarts-2'))
        myChart2.setOption(option2)
        animation()
        window.addEventListener('resize', resize)
        eventBus.on('menuExpand', menuExpand)
        return () => {
            window.removeEventListener('resize', resize)
            eventBus.off('menuExpand', menuExpand)
        }
    }, [])
    return (
        <div className={ cssStyle.echarts }>
            <div className={ cssStyle.echartsContainer }>
                <div id='echarts-1' style={{ height: '400px', width: '50%' }}></div>
                <div id='echarts-2' style={{ height: '400px', width: '50%' }}></div>
            </div>
        </div>
    )
}