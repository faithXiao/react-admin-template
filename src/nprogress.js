import React from 'react'
import { Route } from 'react-router-dom'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import store from '@/store'

class FancyRoute extends React.PureComponent{
    constructor(props){
        super(props)
        NProgress.start()
        console.log(new Date().getTime())
        // store.dispatch({
        //     type: 'NProgress',
        //     value: 'start'
        // })
    }
    componentDidMount(){
        NProgress.done()
        console.log(new Date().getTime())
        // store.dispatch({
        //     type: 'NProgress',
        //     value: 'done'
        // })
    }
    render(){
        return <Route {...this.props} />
    }
}
export default FancyRoute
