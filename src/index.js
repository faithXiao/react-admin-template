// import 'core-js'
import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, HashRouter, Switch } from 'react-router-dom'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import store, { persistor } from '@/store'
import FrontendAuth from './frontendAuth.js'
import '@/style/index.css'
import 'virtual:svg-icons-register'

ReactDOM.render(
	<React.Suspense fallback={ <div></div> }>
		<Provider store={ store }>
			<PersistGate loading={ null } persistor={ persistor }>
				<BrowserRouter basename="/react-admin-template">
					<Switch>
						<FrontendAuth />
					</Switch>
				</BrowserRouter>
			</PersistGate>
		</Provider>
	</React.Suspense>,
	document.getElementById('app')
)