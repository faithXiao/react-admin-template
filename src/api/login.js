import request from '@/request/index'

export function login(data){
    return request({
        url: '/login',
        method: 'post',
        data
    })
}
