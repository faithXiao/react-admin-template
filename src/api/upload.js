import request from '@/request/index'

export function upload(data, onUploadProgress){
    return request({
        url: '/upload',
        method: 'post',
        data,
        headers: {
            'Content-Type': 'multipart/form-data'
        },
        onUploadProgress
    })
}

export function uploadChunk(data, onUploadProgress){
    return request({
        url: '/uploadChunk',
        method: 'post',
        data,
        headers: {
            'Content-Type': 'multipart/form-data'
        },
        onUploadProgress
    })
}

export function merge(data){
    return request({
        url: '/merge',
        method: 'post',
        data
    })
}
