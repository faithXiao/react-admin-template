import request from '@/request/index'

export function searchFriend(data){
    return request({
        url: '/searchFriend',
        method: 'post',
        data
    })
}

export function addFriend(data){
    return request({
        url: '/addFriend',
        method: 'post',
        data
    })
}

export function queryFriendList(){
    return request({
        url: '/queryFriendList',
        method: 'post'
    })
}

export function getHistoryMsg(data){
    return request({
        url: '/getHistoryMsg',
        method: 'post',
        data
    })
}

export function login(data){
    return request({
        url: '/login',
        method: 'post',
        data
    })
}

export function register(data){
    return request({
        url: '/register',
        method: 'post',
        data
    })
}