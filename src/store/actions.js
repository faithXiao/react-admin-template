let actions = {
    updateAccount: function(value){
        return {
            type: 'updateAccount',
            value
        }
    },
    updatePassword: function(value){
        return {
            type: 'updatePassword',
            value
        }
    },
    updateLoginStatus: function(value){
        return {
            type: 'updateLoginStatus',
            value
        }
    },
    login: function(params){
        return function(dispatch){
            return new Promise((resolve, reject) => {
                setTimeout(() => {
                    dispatch(actions.updateLoginStatus({
                        account: params.account,
                        avatar: '',
                        name: params.account
                    }))
                    resolve()
                }, 2000)
            })
        }
    },
    logout: function(){
        return {
            type: 'logout'
        }
    },
    toggleExpand: function(){
        return {
            type: 'toggleExpand'
        }
    }
}

export default actions