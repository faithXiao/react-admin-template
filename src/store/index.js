import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { persistStore, persistReducer } from 'redux-persist'
import storageSession from 'redux-persist/lib/storage/session'
import reducers from './reducers.js'

const persistConfig = {
    key: 'root',
    storage: storageSession
}
const persistedReducer = persistReducer(persistConfig, reducers)

let store = createStore(persistedReducer, applyMiddleware(thunk))
// console.log(store.getState())

// // eslint-disable-next-line no-unused-vars
// const unsubscribe = store.subscribe(
//     () => {
//         console.log(store.getState())
//     }
// )

// unsubscribe()

export const persistor = persistStore(store)
export default store