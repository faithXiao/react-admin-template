import { combineReducers } from 'redux'

const initialState = {
    expand: true,
    account: '123456',
    password: '123456',
    loginInfo: {
        account: '',
        avatar: '',
        name: ''
    }
}

let reducers = combineReducers({
    expand: (state = initialState.expand, action) => {
        if(action.type === 'toggleExpand'){
            return !state
        }
        return state
    },
    account: (state = initialState.account, action) => {
        if(action.type === 'updateAccount'){
            return action.value
        }
        return state
    },
    password: (state = initialState.password, action) => {
        if(action.type === 'updatePassword'){
            return action.value
        }
        return state
    },
    loginInfo: (state = initialState.loginInfo, action) => {
        if(action.type === 'updateLoginStatus'){
            return action.value
        }
        if(action.type === 'logout'){
            return {
                account: '',
                avatar: '',
                name: ''
            }
        }
        return state
    }
})

export default reducers