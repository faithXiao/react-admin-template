class EventBus {
	constructor() {
		this.subscription = []
	}
	on(key, callback) {
		const index = this.subscription.findIndex(item => {
			return item.key === key
		})
		if (index > -1) {
			this.subscription[index].callbackList.push(callback)
		} else {
			this.subscription.push({
				key,
				callbackList: [callback]
			})
		}
	}
	emit(key, value) {
		const index = this.subscription.findIndex(item => {
			return item.key === key
		})
		if (index > -1) {
			this.subscription[index].callbackList.forEach(callback => {
				callback(value)
			})
		}
	}
	off(key, callback) {
		const index = this.subscription.findIndex(item => {
			return item.key === key
		})
		if (index > -1) {
			const _index = this.subscription[index].callbackList.findIndex(item => {
				return item === callback
			})
			this.subscription[index].callbackList.splice(_index, 1)
		}
	}
}

const eventBus = new EventBus()

export default eventBus