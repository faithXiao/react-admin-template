import React from 'react'

export default function SvgIcon({ name, prefix = 'icon', ...props }) {
    const symbolId = `#${prefix}-${name}`
    return (
        <svg {...props} className='icon' aria-hidden="true">
            <use href={symbolId} />
        </svg>
    )
}