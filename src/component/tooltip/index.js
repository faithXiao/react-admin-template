import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import cssStyle from './index.module.css'

class TooltipComponent extends React.Component{
    timer = null
    ref = React.createRef()
    show = (position, placement, title) => {
        if(this.timer){
            clearTimeout(this.timer)
            this.timer = null
        }
        let placementList = ['bottom-left', 'bottom-center', 'bottom-right', 'top-left', 'top-center', 'top-right', 'left-top', 'left-center', 'left-bottom', 'right-top', 'right-center', 'right-bottom']
        if(placementList.indexOf(placement) === -1){
            console.error('Invalid props: placement')
        } else {
            let mainRef = this.ref.current
            let containerRef = mainRef.getElementsByTagName('span')[0]
            let triangleRef = mainRef.getElementsByTagName('span')[1]
            mainRef.style.visibility = 'visible'
            containerRef.innerHTML = title
            mainRef.style.opacity = 1
            let { width, height, top, left, bottom } = position
            let placementDecomposition = placement.split('-')
            if(placementDecomposition[0] === 'bottom'){
                mainRef.style.top = bottom + 5 + 'px'
                triangleRef.style.left = '50%'
                triangleRef.style.right = 'unset'
                triangleRef.style.top = '-5px'
                triangleRef.style.bottom = 'unset'
                triangleRef.style.transform = 'translateX(-50%) rotate(0)'
            }
            if(placementDecomposition[0] === 'top'){
                mainRef.style.top = top - mainRef.offsetHeight - 5 + 'px'
                triangleRef.style.left = '50%'
                triangleRef.style.right = 'unset'
                triangleRef.style.top = 'unset'
                triangleRef.style.bottom = '-5px'
                triangleRef.style.transform = 'translateX(-50%) rotate(180deg)'
            }
            if(placementDecomposition[0] === 'left'){
                mainRef.style.left = left - mainRef.offsetWidth - 5 + 'px'
                triangleRef.style.left = 'unset'
                triangleRef.style.right = '-7px'
                triangleRef.style.top = '50%'
                triangleRef.style.bottom = 'unset'
                triangleRef.style.transform = 'translateY(-50%) rotate(90deg)'
            }
            if(placementDecomposition[0] === 'right'){
                mainRef.style.left = left + width + 5 + 'px'
                triangleRef.style.left = '-7px'
                triangleRef.style.right = 'unset'
                triangleRef.style.top = '50%'
                triangleRef.style.bottom = 'unset'
                triangleRef.style.transform = 'translateY(-50%) rotate(-90deg)'
            }
            if(placementDecomposition[0] === 'bottom' || placementDecomposition[0] === 'top'){
                if (placementDecomposition[1] === 'left') {
                    mainRef.style.left = left + 'px'
                }
                if (placementDecomposition[1] === 'center') {
                    mainRef.style.left = left + width/2 - mainRef.offsetWidth/2 + 'px'
                }
                if (placementDecomposition[1] === 'right') {
                    mainRef.style.left = left + width - mainRef.offsetWidth + 'px'
                }
            }
            if(placementDecomposition[0] === 'left' || placementDecomposition[0] === 'right'){
                if (placementDecomposition[1] === 'top') {
                    mainRef.style.top = top + 'px'
                }
                if (placementDecomposition[1] === 'center') {
                    mainRef.style.top = top + height/2 - mainRef.offsetHeight/2 + 'px'
                }
                if (placementDecomposition[1] === 'bottom') {
                    mainRef.style.top = top + height - mainRef.offsetHeight + 'px'
                }
            }
        }
    }
    hide = () => {
        this.ref.current.style.opacity = 0
        this.timer = setTimeout(() => {
            if(this.ref.current){
                this.ref.current.style.visibility = 'hidden'
            }
        }, 300)
    }
    mouseEnter = () => {
        this.show(this.props.position, this.props.placement, this.props.title)
    }
    mouseLeave = () => {
        this.hide()
    }
    componentDidUpdate(){
        this.show(this.props.position, this.props.placement, this.props.title)
    }
    componentDidMount(){
        setTimeout(() => {
            this.show(this.props.position, this.props.placement, this.props.title)
        })
    }
    render(){
        return(
            <div ref={ this.ref } className={ cssStyle.tooltip } onMouseEnter={ this.mouseEnter } onMouseLeave={ this.mouseLeave }>
                <span></span>
                <span className={ cssStyle.triangle + ' ' + cssStyle.triangleUp }></span>
            </div>
        )
    }
}

TooltipComponent.propTypes = {
    placement: PropTypes.string,
    title: PropTypes.string,
    position: PropTypes.object
}
TooltipComponent.defaultProps = {
    placement: 'bottom-center'
}

let container = null, ref = null

export default class Tooltip extends React.Component{
    toggle = position => {
        if (!container) {
            container = document.createElement('div')
            document.body.appendChild(container)
        }
        ref = ReactDOM.render(
            <TooltipComponent {...this.props} position={ position }/>,
            container
        )
    }
    mouseEnter = e => {
        if(this.props.children.props.onMouseEnter){
            this.props.children.props.onMouseEnter()
        }
        if (!this.props.hide) {
            let position = e.currentTarget.getBoundingClientRect()
            this.toggle(position)
        }
    }
    mouseLeave = () => {
        if(this.props.children.props.onMouseLeave){
            this.props.children.props.onMouseLeave()
        }
        if (!this.props.hide && ref) {
            ref.hide()
        }
    }
    componentDidUpdate() {
        if (this.props.hide && ref) {
            ref.hide()
        }
    }
    componentWillUnmount() {
        if (ref) {
            ref.hide()
        }
    }
    render(){
        return React.Children.map(this.props.children, item => {
            if(item){
                return React.cloneElement(item, {
                    onMouseEnter: this.mouseEnter,
                    onMouseLeave: this.mouseLeave
                })
            }
        })
    }
}

Tooltip.propTypes = {
    children: PropTypes.object,
    hide: PropTypes.bool
}