import React, { useEffect, useState } from "react"
import PropTypes from 'prop-types'
import cssStyle from './index.module.css'

export default function Progress(props) {
    const [timer, setTimer] = useState(null)
    const [strokeWidth, setStrokeWidth] = useState(4.8)
    useEffect(() => {
        if (props.type === 'circle') {
            if (timer) {
                clearTimeout(timer)
                setTimer(null)
            }
            if (props.percent === 0) {
                setTimer(setTimeout(() => {
                    setStrokeWidth(0)
                }, 600))
            } else {
                setStrokeWidth(4.8)
            }
        }
    }, [props.percent])
    let activeColor = ''
    if (Array.isArray(props.activeColor)) {
        for (let i = props.activeColor.length - 1; i >= 0; i -= 1) {
            if (props.percent >= props.activeColor[i].percent) {
                activeColor = props.activeColor[i].color
                break
            }
        }
    } else {
        activeColor = props.activeColor
    }
    const svgStyle = {
        width: props.size + 'px',
        height: props.size + 'px',
        position: 'relative'
    }
    const lineBoxStyle = {
        height: props.strokeWidth + 'px',
        backgroundColor: props.inActiveColor,
        borderRadius: props.strokeWidth + 'px'
    }
    const lineContentStyle = {
        width: props.percent + '%',
        height: '100%',
        backgroundColor: props.stripe ? undefined : activeColor,
        borderRadius: props.strokeWidth + 'px',
        backgroundImage: props.stripe ? `repeating-linear-gradient(-45deg, ${props.activeColor[0]} 25%, ${props.activeColor[1]} 0, ${props.activeColor[1]} 50%, ${props.activeColor[0]} 0, ${props.activeColor[0]} 75%, ${props.activeColor[1]} 0)` : undefined,
        animation: props.stripe ? `${cssStyle.panoramic} ${props.percent/100*30}s linear infinite` : undefined
    }
    const size = 295.31
    return (
        <div className={ cssStyle.progress }>
            {
                props.type === 'circle'
                ?
                <div style={ svgStyle }>
                    <svg viewBox="0 0 100 100">
                        <path d="
                            M 50 50
                            m 0 -47
                            a 47 47 0 1 1 0 94
                            a 47 47 0 1 1 0 -94
                            " stroke={ props.inActiveColor } strokeWidth="4.8" fill="none" style={{ strokeDasharray: `${size}px, ${size}px`, strokeDashoffset: '0px' }}>
                        </path>
                        <path d="
                            M 50 50
                            m 0 -47
                            a 47 47 0 1 1 0 94
                            a 47 47 0 1 1 0 -94
                            " stroke={ activeColor } fill="none" strokeLinecap="round" strokeWidth={ strokeWidth } style={{ strokeDasharray: `${props.percent/100*size}px, ${size}px`, strokeDashoffset: '0px', transition: 'stroke-dasharray 0.6s ease 0s, stroke 0.6s ease 0s' }}>
                        </path>
                    </svg>
                    <div className={ cssStyle.container } style={{ width: props.size - 20 + 'px', height: props.size - 20 + 'px' }}>{ props.children }</div>
                </div>
                :
                (
                    (props.position === 'end' || !props.position)
                    ?
                    <div className={ cssStyle.endLine }>
                        <div className={ cssStyle.line } style={ lineBoxStyle }>
                            <div style={ lineContentStyle } className={ cssStyle.lineContent }></div>
                        </div>
                        {
                            props.children
                            ?
                            <div className={ cssStyle.render }>{ props.children }</div>
                            :
                            null
                        }
                    </div>
                    :
                    (
                        props.position === 'right'
                        ?
                        <div className={ cssStyle.rightLine } style={ lineBoxStyle }>
                            <div className={ cssStyle.line + ' ' + cssStyle.lineContent } style={ lineContentStyle }>
                            {
                                props.children
                                ?
                                <div className={ cssStyle.render }>{ props.children }</div>
                                :
                                null
                            }
                            </div>
                        </div>
                        :
                        <div className={ cssStyle.centerLine }>
                            <div className={ cssStyle.line } style={ lineBoxStyle }>
                                <div style={ lineContentStyle } className={ cssStyle.lineContent }></div>
                            </div>
                            {
                                props.children
                                ?
                                <div className={ cssStyle.render }>{ props.children }</div>
                                :
                                null
                            }
                        </div>
                    )
                )
            }
        </div>
    )
}

Progress.propTypes = {
    activeColor: PropTypes.oneOfType([PropTypes.array, PropTypes.string]),
    inActiveColor: PropTypes.string,
    percent: PropTypes.number,
    size: PropTypes.number,
    thickness: PropTypes.number,
    children: PropTypes.object,
    type: PropTypes.string,
    position: PropTypes.string
}

Progress.defaultProps = {
    activeColor: 'red',
    inActiveColor: '#e5e9f2',
    percent: 50,
    size: 150,
    position: 'end'
}