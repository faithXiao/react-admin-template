import cssStyle from './index.module.css'
import ReactDOM from 'react-dom'
import React from 'react'
import { Button, Model } from '../index'
import close from '../../assets/images/close.png'
import PropTypes from 'prop-types'

class AlertComponent extends React.PureComponent{
    close = this.close.bind(this)
    modelRef = React.createRef()
    contentRef = React.createRef()
    componentDidMount(){
        setTimeout(() => {  //setTimeout保证弹出动画能够正常执行
            //弹出动画
            this.modelRef.current.open()
            this.contentRef.current.style.transform = 'translateY(0)'
            this.contentRef.current.style.opacity = 1
            window.addEventListener('hashchange', this.close)
        })
    }
    componentWillUnmount(){  //页面路由改变关闭弹窗
        window.removeEventListener('hashchange', this.close)
    }
    close(){  //弹窗关闭事件动画
        window.removeEventListener('hashchange', this.close)
        this.modelRef.current.close()
        this.contentRef.current.style.transform = 'translateY(-100%)'
        this.contentRef.current.style.opacity = 0
        setTimeout(() => {
            this.props.close()
        }, 300)
    }
    confirm(){  //确认事件
        this.close()
        if(this.props.confirm){
            this.props.confirm()  //执行确认回调
        }
    }
    cancel(){  //取消事件
        this.close()
        if(this.props.cancel){
            this.props.cancel()  //执行取消回调
        }
    }
    render(){
        return(
            <Model ref={ this.modelRef } onClick={ this.close }>
                <div className={ cssStyle.content } ref={ this.contentRef } onClick={ e => { e.stopPropagation() } }>
                    <div className={ cssStyle.head }>
                        <h4>{ this.props.title }</h4>
                        <img src={ close } alt="close" onClick={ this.close } />
                    </div>
                    <div className={ cssStyle.body }>
                        { this.props.message }
                    </div>
                    <div className={ cssStyle.select }>
                        <Button type='default' onClick={ this.cancel.bind(this) } style={{ marginRight: '10px' }}>取消</Button>
                        <Button type='primary' onClick={ this.confirm.bind(this) }>确定</Button>
                    </div>
                </div>
            </Model>
        )
    }
}

AlertComponent.propTypes = {
    close: PropTypes.func,
    confirm: PropTypes.func,
    cancel: PropTypes.func,
    title: PropTypes.string,
    message: PropTypes.string
}

function Alert(){
    let alertContainer = null
    function hideAlert(){
        if(alertContainer){
            ReactDOM.unmountComponentAtNode(alertContainer)
            document.body.removeChild(alertContainer)
            alertContainer = null
        }
    }
    return{
        showAlert(props){
            alertContainer = document.createElement('div')
            document.body.appendChild(alertContainer)
            ReactDOM.render(
                <AlertComponent {...props} close={ hideAlert } />,
                alertContainer
            )
        }
    }
}

export default Alert