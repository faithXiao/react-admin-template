import React from 'react'
import ReactDOM from 'react-dom'
import cssStyle from './index.module.css'
import close from '../../assets/images/close.png'
import { Model } from '../index'
import PropTypes from 'prop-types'

class DialogComponent extends React.PureComponent{
    constructor(props){
        super(props)
        this.modelRef = React.createRef()
        this.contentRef = React.createRef()
    }
    open = () =>{
        this.modelRef.current.open()
        this.contentRef.current.style.transform = 'translateY(0)'
        this.contentRef.current.style.opacity = 1
    }
    close = () =>{
        this.modelRef.current.close()
        this.contentRef.current.style.transform = 'translateY(-100%)'
        this.contentRef.current.style.opacity = 0
        if(this.props.destroy){
            setTimeout(() => {
                this.props.remove()
            }, 300)
        }
    }
    componentDidUpdate(){
        if(this.props.show){
            window.requestAnimationFrame(() => {
                setTimeout(() => {
                    this.open()
                })
            })
        }else{
            window.requestAnimationFrame(() => {
                setTimeout(() => {
                    this.close()
                })
            })
        }
    }
    remove = () => {
        this.props.remove()
    }
    componentDidMount(){
        window.requestAnimationFrame(() => {
            setTimeout(() => {
                this.open()
            })
        })
        window.addEventListener('hashchange', this.remove)
    }
    componentWillUnmount(){
        window.removeEventListener('hashchange', this.remove)
    }
    render(){
        return (
            <Model ref={ this.modelRef } onClick={ this.props.onCancel }>
                <div className={ cssStyle.content } ref={ this.contentRef } style={{ width: this.props.width || 'unset' }}>
                    <div className={ cssStyle.head }>
                        <h4>{ this.props.title }</h4>
                        <img src={ close } alt="close" onClick={ this.props.onCancel } />
                    </div>
                    <div className={ cssStyle.body }>
                        { this.props.children }
                    </div>
                </div>
            </Model>
        )
    }
}

DialogComponent.propTypes = {
    onCancel: PropTypes.func,
    width: PropTypes.string,
    minWidth: PropTypes.string,
    title: PropTypes.string,
    children: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    show: PropTypes.bool,
    remove: PropTypes.func,
    destroy: PropTypes.bool
}

class Dialog extends React.PureComponent{
    constructor(props){
        super(props)
        this.dialogContainer = null
        this.show = false
    }
    remove = () => {
        ReactDOM.unmountComponentAtNode(this.dialogContainer)
        document.body.removeChild(this.dialogContainer)
        this.dialogContainer = null
    }
    componentDidUpdate(){
        if(this.props.show){
            this.show = this.props.show
            if(!this.dialogContainer){
                this.dialogContainer = document.createElement('div')
                document.body.appendChild(this.dialogContainer)
            }
        }else{
            if(this.show && this.props.beforeClose !== undefined && typeof this.props.beforeClose === 'function'){
                this.props.beforeClose()
            }
            this.show = this.props.show
        }
        if(this.dialogContainer){
            ReactDOM.render(
                <DialogComponent {...this.props} remove={ this.remove } />,
                this.dialogContainer
            )
        }
    }
    render(){
        return(
            null
        )
    }
}

Dialog.propTypes = {
    show: PropTypes.bool,
    beforeClose: PropTypes.func,
    destroy: PropTypes.bool
}

export default Dialog