import Alert from './alert'
import Toask from './toask'
import Dialog from './dialog'
import Button from './button'
import Loading from './loading'
import Tag from './tag'
import Table from './table'
import Pagination from './pagination'
import Form from './form'
import Model from './model'
import Tree from './tree'
import Crop from './crop'
import Tooltip from './tooltip'
import Carousel from './carousel'
import Progress from './progress'
import Transfer from './transfer'
import Notification from './notification'

import '@/component/assets/icon/iconfont.css'

export {
    Alert,
    Toask,
    Dialog,
    Button,
    Loading,
    Tag,
    Table,
    Pagination,
    Form,
    Model,
    Tree,
    Crop,
    Tooltip,
    Carousel,
    Progress,
    Transfer,
    Notification
}