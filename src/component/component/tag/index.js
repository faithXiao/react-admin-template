import React from 'react'
import cssStyle from './index.module.css'

let btnBg = {
    'default': {
        backgroundColor: 'white'
    },
    'primary': {
        color: '#fff',
        backgroundColor: '#409eff'
    },
    'success': {
        color: '#67c23a',
        background: '#f0f9eb',
        borderColor: '#c2e7b0'
    },
    'info': {
        color: '#909399',
        background: '#f4f4f5',
        borderColor: '#d3d4d6'
    },
    'warning': {
        color: '#e6a23c',
        background: '#fdf6ec',
        borderColor: '#f5dab1'
    },
    'error': {
        color: '#f56c6c',
        background: '#fef0f0',
        borderColor: '#fbc4c4'
    }
}

let btnSize = {
    'mini': {
        padding: '2px 6px',
        fontSize: '12px'
    },
    'small': {
        padding: '4px 8px',
        fontSize: '12px'
    },
    'medium': {
        padding: '6px 10px'
    }
}

export default function tag(props){
    let { type, size, style, className, children } = props
    let typeStyle = btnBg[type || 'default']
    let sizeStyle = size ? btnSize[size] : {}
    let _style = {
        ...typeStyle,
        ...sizeStyle,
        ...style
    }
    return <span {...props} className={ className !== undefined ? (className + ' ' + cssStyle.tag) : cssStyle.tag } style={ _style }>{ children }</span>
}