import React from 'react'
import PropTypes from 'prop-types'
import cssStyle from './index.module.css'

const context = React.createContext()

class CheckboxGroup extends React.PureComponent{
    onChange = value => {
        let propsValue = [...this.props.value]
        let index = -1
        for(let i in this.props.value){
            if(this.props.value[i] == value){
                index = i
                break
            }
        }
        if(index > -1){
            propsValue.splice(index, 1)
        }else{
            propsValue.push(value)
        }
        if(this.props.onChange){
            this.props.onChange(propsValue)
        }
    }
    render(){
        let invalidStyle = {}
        if(this.props.invalid){
            invalidStyle.border = '1px solid red'
            invalidStyle.paddingLeft = '12px'
        }
        return(
            <context.Provider value={{
                onChange: this.onChange,
                value: this.props.value
            }}>
                <div style={{ borderRadius: '4px', ...invalidStyle }}>
                    { this.props.children }
                </div>
            </context.Provider>
        )
    }
}

CheckboxGroup.propTypes = {
    onChange: PropTypes.func,
    value: PropTypes.array,
    children: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    invalid: PropTypes.bool
}

class Checkbox extends React.PureComponent{
    static contextType = context
    onClick = e => {
        e.stopPropagation()
        if (this.props.disabled) {
            return
        }
        if (this.context && this.context.onChange && this.props.value !== undefined) {
            this.context.onChange(this.props.value)
        }
        if(this.props.onChange){
            this.props.onChange()
        }
    }
    render(){
        let checked = this.props.checked || false
        if (this.context && this.context.value && this.props.value) {
            checked = this.context.value.some(item => {
                return item === this.props.value
            })
        }
        return(
            <div className={ cssStyle.radio + (this.props.disabled ? ' ' + cssStyle.disabled : '') } style={ this.props.style || {} } onClick={ this.onClick }>
                <div style={{ display: 'flex', alignItems: 'center' }}>
                    <span className={ cssStyle.checkbox + ' ' + (checked ? cssStyle.checked : '') }>
                        <span className={ cssStyle.inner }></span>
                    </span>
                    {
                        this.props.label
                        ?
                        <span className={ this.props.disabled ? cssStyle.disabled : '' } style={{ color: checked ? '#409eff' : '#000', marginLeft: '8px', marginRight: '12px' }}>{ this.props.label }</span>
                        :
                        null
                    }
                </div>
            </div>
        )
    }
}

Checkbox.propTypes = {
    onChange: PropTypes.func,
    checked: PropTypes.bool,
    label: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    disabled: PropTypes.bool,
    style: PropTypes.object
}

export {
    CheckboxGroup,
    Checkbox
}