import React from 'react'
import cssStyle from './index.module.css'
import PropTypes from 'prop-types'

let btnBg = {
    'default': {
        backgroundColor: 'white'
    },
    'default-pain': {
        background: '#ecf5ff',
        borderColor: '#b3d8ff'
    },
    'primary': {
        color: 'white',
        backgroundColor: '#409eff',
        borderColor: '#409eff'
    },
    'primary-pain': {
        color: '#409eff',
        background: '#ecf5ff',
        borderColor: '#b3d8ff'
    },
    'success': {
        color: 'white',
        backgroundColor: '#67c23a',
        borderColor: '#67c23a'
    },
    'success-pain': {
        color: '#67c23a',
        background: '#f0f9eb',
        borderColor: '#c2e7b0'
    },
    'info': {
        color: 'white',
        backgroundColor: '#909399',
        borderColor: '#909399'
    },
    'info-pain': {
        color: '#909399',
        background: '#f4f4f5',
        borderColor: '#d3d4d6'
    },
    'warning': {
        color: 'white',
        backgroundColor: '#e6a23c',
        borderColor: '#e6a23c'
    },
    'warning-pain': {
        color: '#e6a23c',
        background: '#fdf6ec',
        borderColor: '#f5dab1'
    },
    'error': {
        color: 'white',
        backgroundColor: '#f56c6c',
        borderColor: '#f56c6c'
    },
    'error-pain': {
        color: '#f56c6c',
        background: '#fef0f0',
        borderColor: '#fbc4c4'
    }
}

let btnSize = {
    'mini': {
        padding: '6px 12px',
        fontSize: '12px'
    },
    'small': {
        padding: '8px 15px',
        fontSize: '12px'
    },
    'medium': {
        padding: '10px 20px'
    }
}

export default function Button(props){
    let typeStyle = btnBg[props.type || 'default']
    let painStyle = btnBg[props.type ? (props.pain ? (props.type + '-pain') : 'default' ) : 'default']
    let sizeStyle = props.size ? btnSize[props.size] : {}
    let style = props.pain ? painStyle : typeStyle
    style = {
        ...style,
        ...sizeStyle,
        ...props.style
    }
    // eslint-disable-next-line no-unused-vars
    let { pain, ...rest } = props
    return (
        <button
            {...rest}
            type="button"
            className={ cssStyle.btn + '' + (rest.disabled ? (' ' + cssStyle.disabled) : '')}
            style={ style }
        >
            { props.children }
        </button>
    )
}

Button.propTypes = {
    children: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
    style: PropTypes.object,
    type: PropTypes.string,
    pain: PropTypes.bool,
    size: PropTypes.string,
    className: PropTypes.string
}