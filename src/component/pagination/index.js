import React from 'react'
import { Button, Toask } from '@/component/index'
import cssStyle from './index.module.css'
import PropTypes from 'prop-types'

export default class Pagination extends React.PureComponent{
    inputRef = React.createRef()
    prevNext(type){
        if(type === 'prev'){
            if(this.props.current === 1){
                return
            }else{
                let current = this.props.current
                current--
                this.swicthPage(current)
            }
        }else{
            let pageCount = Math.ceil(this.props.total / this.props.size)
            if(this.props.current === pageCount){
                return
            }else{
                let current = this.props.current
                current++
                this.swicthPage(current)
            }
        }
    }
    swicthPage(page){
        if(page === this.props.current){
            return
        }
        this.props.currentChange(page)
    }
    inputChange(){
        let page = parseInt(this.inputRef.current.value) || 1
        let pageCount = Math.ceil(this.props.total / this.props.size)
        if(page > pageCount){
            let toask = Toask()
            toask.showToask({
                title: '超过最大页码数',
                type: 'error',
                duration: 1500
            })
            return
        }
        this.swicthPage(page)
    }
    onChange(e){
        this.inputRef.current.value = e.target.value.replace(/^(0+)|[^\d]+/g,'')
    }
    componentDidUpdate(){
        this.inputRef.current.value = this.props.current
    }
    componentDidMount(){
        this.inputRef.current.value = this.props.current
    }
    render(){
        let pageCount = Math.ceil(this.props.total / this.props.size)
        let current = this.props.current
        let count = this.props.count
        let arr = []
        if(pageCount > count){
            arr = new Array(this.props.count).fill(null)
            let start = 1
            if(current <= Math.ceil(this.props.count/2)){
                start = 1
            }else{
                if(current < (pageCount - Math.floor(this.props.count/2) + 1)){
                    if(count % 2 === 0){
                        start = current - Math.floor(this.props.count/2) + 1
                    }else{
                        start = current - Math.floor(this.props.count/2)
                    }
                }else{
                    start = pageCount - this.props.count + 1
                }
            }
            arr = arr.map(() => {
                return start++
            })
        }else{
            arr = new Array(pageCount).fill(null)
            let start = 1
            arr = arr.map(() => {
                return start++
            })
        }
        if(!arr.length){
            arr = [1]
        }
        const justifyContent = {
            left: 'flex-start',
            center: 'center',
            right: 'flex-end'
        }
        return(
            <div className={ cssStyle.pagination } style={{
                    justifyContent: justifyContent[this.props.position] || 'flex-start',
                    ...this.props.style
                }}
            >
                <div className={ cssStyle.item }>
                    <span onClick={ (() => { this.prevNext('prev') }).bind(this) }>&lt;</span>
                    {
                        arr.map((item, index) =>{
                            return(
                                <span key={ index } className={ this.props.current === item ? cssStyle.active : '' } onClick={ (() => { this.swicthPage(item) }).bind(this) }>{ item }</span>
                            )
                        })
                    }
                    <span onClick={ (() => { this.prevNext('next') }).bind(this) }>&gt;</span>
                    <input type="text" className={ cssStyle.input } ref={ this.inputRef } onChange={ this.onChange.bind(this) } />
                    <Button type="primary" size="mini" style={{ padding: '6px' }} onClick={ this.inputChange.bind(this) }>go</Button>
                    <p style={{ fontSize: '14px', marginLeft: '10px' }}>共{ pageCount }页</p>
                </div>
            </div>
        )
    }
}

Pagination.propTypes = {
    current: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    total: PropTypes.number,
    size: PropTypes.number,
    currentChange: PropTypes.func,
    count: PropTypes.number,
    style: PropTypes.object,
    position: PropTypes.string
}