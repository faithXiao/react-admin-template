import cssStyle from './index.module.css'
import ReactDOM from 'react-dom'
import React from 'react'

const notificationList = []

class NotificationComponent extends React.PureComponent{
    close = this.close.bind(this)
    contentRef = React.createRef()
    messageRef = React.createRef()
    componentDidMount(){
        this.messageRef.current.innerHTML = this.props.message
        setTimeout(() => {  //setTimeout保证弹出动画能够正常执行
            //弹出动画
            this.contentRef.current.style.transform = 'translateX(0)'
            this.contentRef.current.style.opacity = 1
        })
        if (this.props.duration) {
            setTimeout(() => {
                this.close()
            }, this.props.duration)
        }
    }
    close(){  //弹窗关闭事件动画
        if (this.contentRef.current) {
            this.contentRef.current.style.transform = this.props.position === 'bottom-left' || this.props.position === 'top-left' ? 'translateX(-100%)' : 'translateX(100%)'
            this.contentRef.current.style.opacity = 0
            this.props.close()
        }
    }
    render(){
        return(
            <div className={ cssStyle.content } ref={ this.contentRef } style={{ transform: this.props.position === 'bottom-left' || this.props.position === 'top-left' ? 'translateX(-100%)' : 'translateX(100%)' }} onClick={ e => { e.stopPropagation() } }>
                { !this.props.duration && <span className={ cssStyle.close } onClick={ this.close }>x</span> }
                <div className={ cssStyle.body }>
                    <h4 className={ cssStyle.title }>{ this.props.title }</h4>
                    <div className={ cssStyle.message } ref={ this.messageRef }></div>
                </div>
                { this.props.footer && <div className={ cssStyle.footer }>{ this.props.footer }</div> }
            </div>
        )
    }
}

function Notification(){
    let timer = null
    let notificationContainer = null
    let componentRef = null
    const close = () => {
        if (componentRef) {
            if(timer){
                return
            }
            timer = setTimeout(() => {
                const index = notificationList.findIndex(item => item.dom === notificationContainer)
                if (index > -1) {
                    notificationList.splice(index, 1)
                }
                ReactDOM.unmountComponentAtNode(notificationContainer)
                document.body.removeChild(notificationContainer)
                notificationContainer = null
                componentRef = null
                timer = null
            }, 300)
        }
    }
    const notify = props => {
        notificationContainer = document.createElement('div')
        let style = 'position: fixed; z-index: 99999; transition: all .3s; '
        if (!props.position || props.position === 'bottom-right') {
            style += 'right: 20px; bottom: 20px'
        }
        if (props.position === 'top-right') {
            style += 'right: 20px; top: 20px'
        }
        if (props.position === 'top-left') {
            style += 'left: 20px; top: 20px'
        }
        if (props.position === 'bottom-left') {
            style += 'left: 20px; bottom: 20px'
        }
        notificationContainer.style = style
        document.body.appendChild(notificationContainer)
        componentRef = ReactDOM.render(
            <NotificationComponent {...props}
                close={
                    () => {
                        close()
                        if (props.close) {
                            props.close()
                        }
                    }
                }
            />,
            notificationContainer
        )
        for (let i = 0; i < notificationList.length; i += 1) {
            if ((notificationList[i].position === 'bottom-right' && (!props.position || props.position === 'bottom-right')) || (notificationList[i].position === 'bottom-left' && props.position === 'bottom-left')) {
                const bottom = parseFloat(notificationList[i].dom.style.bottom)
                notificationList[i].dom.style.bottom = bottom + notificationContainer.offsetHeight + 20 + 'px'
            }
            if ((notificationList[i].position === 'top-right' && props.position === 'top-right') || (notificationList[i].position === 'top-left' && props.position === 'top-left')) {
                const top = parseFloat(notificationList[i].dom.style.top)
                notificationList[i].dom.style.top = top + notificationContainer.offsetHeight + 20 + 'px'
            }
        }
        notificationList.push({
            dom: notificationContainer,
            position: props.position || 'bottom-right'
        })
    }
    return {
        close,
        notify
    }
}

export default Notification