import React from 'react'
import PropTypes from 'prop-types'
import cssStyle from './index.module.css'

class Input extends React.PureComponent{
    constructor(props){
        super(props)
    }
    onChange(value){
        if(this.props.onChange){
            this.props.onChange(value, this.props)
        }
    }
    render(){
        let { value, invalid, style, ...rest } = this.props
        let invalidStyle = {}
        if(invalid){
            invalidStyle.border = '1px solid red'
        }
        let _style = {
            ...invalidStyle
        }
        if(style){
            _style = {
                ..._style,
                ...style
            }
        }
        let Com = rest.type === 'textarea' ? 'textarea' : 'input'
        return(
            <Com
                { ...rest }
                style={ _style }
                value={ ((typeof value === 'string') || (typeof value === 'number') ) ? value : '' }
                onChange={ (e => { this.onChange(e.target.value) }).bind(this) }
                className={ cssStyle.input + '' + (rest.disabled ? (' ' + cssStyle.disabled) : '') + (this.props.className ? (' ' + this.props.className) : '') }
            />
        )
    }
}

Input.propTypes = {
    onChange: PropTypes.func,
    className: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    type: PropTypes.string,
    disabled: PropTypes.bool,
    placeholder: PropTypes.string,
    style: PropTypes.object,
    labelWidth: PropTypes.string,
    invalid: PropTypes.bool
}

export {
    Input
}