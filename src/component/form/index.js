import { Form, Item } from "./form.js"
import { Input } from "./input.js"
import { RadioGroup, Radio } from "./radio.js"
import { CheckboxGroup, Checkbox } from "./checkbox.js"
import { Select, Option } from "./select.js"
import { Switch } from "./switch.js"
import { Upload } from "./upload.js"
import { TimePicker } from "./timePicker.js"
import { Calendar } from "./calendar.js"

export default {
    form: Form,
    item: Item,
    input: Input,
    radioGroup: RadioGroup,
    radio: Radio,
    checkboxGroup: CheckboxGroup,
    checkbox: Checkbox,
    select: Select,
    option: Option,
    switch: Switch,
    upload: Upload,
    timePicker: TimePicker,
    calendar: Calendar
}