import React from 'react'
import cssStyle from './index.module.css'
import PropTypes from 'prop-types'

class Switch extends React.PureComponent{
    onChange(){
        if(!this.props.disabled){
            if(this.props.onChange){
                this.props.onChange(!this.props.value, this.props)
            }
        }
    }
    render(){
        let isOpen = this.props.value
        let disabled = this.props.disabled
        return(
            <div
                className={ cssStyle.switch + '' + (disabled ? (' ' + cssStyle.disabled) : '') }
                onClick={ this.onChange.bind(this) }
            >
                <div
                    className={ cssStyle.block }
                    style={{
                        background: isOpen ? 'rgb(19, 206, 102)' : 'rgb(255, 73, 73)'
                    }}
                >
                    <span style={{ left: isOpen ? '22px' : '2px' }}></span>
                </div>
                <span>{ this.props.value ? this.props.activeText : this.props.inactiveText }</span>
            </div>
        )
    }
}

Switch.propTypes = {
    onChange: PropTypes.func,
    value: PropTypes.bool,
    activeText: PropTypes.string,
    inactiveText: PropTypes.string,
    disabled: PropTypes.bool,
    style: PropTypes.object
}

export {
    Switch
}