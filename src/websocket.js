import { io } from "socket.io-client"
import eventBus from '@/util/eventBus'
import store from '@/store'

const socket = io("ws://localhost:3000", {
    auth: {
        token: store.getState().loginInfo.account
    }
})
socket.on("connect", () => {
    console.log(socket.id)
})
socket.on("msg", data => {
    eventBus.emit('msg', data)
})

export default socket