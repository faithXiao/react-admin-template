import axios from 'axios'
import store from '@/store'
import { Toask } from "@/component/index"

const service = axios.create({
    baseURL: import.meta.env.VITE_BASIC_API,
    timeout: 0 // 请求超时时间
})

// request拦截器
service.interceptors.request.use(
    config => {
        config.headers['token'] = store.getState().loginInfo.account
        return config
    },
    error => {
        console.log(config)
        return Promise.reject(error)
    }
)

// respone拦截器
service.interceptors.response.use(
    response => {
        console.log(response)
        if (!response.data.status) {
            let toask = Toask()
            toask.showToask({
                type: 'error',
                title: response.data.msg,
                duration: 1500
            })
        }
        return Promise.resolve(response.data)
    },
    error => {
        console.log(error)
        return Promise.reject(error)
    }
)

export default service
