import React from 'react'
import { Route, Redirect } from 'react-router-dom'

class AsyncRoute extends React.Component{
    state = {
        component: null
    }
    componentDidMount() {
        console.log('AsyncRoute-componentDidMount')
        console.log(this.props.currentRoute.component)
    }
    render(){
        console.log('AsyncRoute-render')
        const { beforeEnter, afterEnter, currentRoute } = this.props
        return (
            <Route
                render={
                    props => {
                        console.log(currentRoute, 'currentRoute')
                        if(currentRoute.component){
                            return <currentRoute.component {...this.props} {...props} routesTable={ currentRoute.children } route={ currentRoute } />
                        }
                        if(currentRoute.redirect){
                            return <Redirect to={ currentRoute.redirect } />
                        }
                        return null
                    }
                }
            />
        )
    }
}

export default AsyncRoute