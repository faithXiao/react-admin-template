import React from 'react'
import Layout from '@/views/layout'

const staticRoutes = [
    {
        path: "/login",
        component: React.lazy(() => import('@/views/login/index')),
        name: 'login',
        hidden: true
    },
    {
        path: "/register",
        component: React.lazy(() => import('@/views/register/index')),
        name: 'register',
        hidden: true
    },
    {
        path: "/404",
        component: React.lazy(() => import('@/views/404/index')),
        name: '404',
        hidden: true
    }
]

const routes = [
    {
        path: "/",
        redirect: '/home',
        hidden: true
    },
    {
        path: "/home",
        redirect: '/home/index',
        component: Layout,
        name: 'Home',
        icon: 'home',
        children: [
            {
                path: "index",
                component: React.lazy(() => import('@/views/home/index')),
                name: 'Home'
            }
        ]
    },
    {
        path: "/list",
        component: Layout,
        name: '一级导航',
        icon: 'list',
        children: [
            {
                path: 'a',
                component: React.lazy(() => import('@/views/list/a/index')),
                name: '二级导航',
                icon: 'list',
                children: [
                    {
                        path: '1',
                        component: React.lazy(() => import('@/views/list/a/1/index')),
                        icon: 'list',
                        name: '三级页面-1'
                    },
                    {
                        path: '2',
                        component: React.lazy(() => import('@/views/list/a/2/index')),
                        icon: 'list',
                        name: '三级页面-2'
                    }
                ]
            },
            {
                path: 'b',
                component: React.lazy(() => import('@/views/list/b/index')),
                icon: 'list',
                name: '二级页面'
            },
            {
                path: 'c',
                component: React.lazy(() => import('@/views/list/c/index')),
                icon: 'list',
                name: '路由传参'
            },
            {
                path: 'params/:user/:id',
                component: React.lazy(() => import('@/views/list/params/index')),
                name: '路由参数',
                hidden: true
            }
        ]
    },
    {
        path: "/dialog",
        redirect: '/dialog/index',
        component: Layout,
        name: 'Dialog',
        icon: 'dialog',
        children: [
            {
                path: 'index',
                component: React.lazy(() => import('@/views/dialog/index')),
                name: 'Dialog'
            }
        ]
    },
    {
        path: "/button",
        redirect: '/button/index',
        component: Layout,
        name: 'Button',
        icon: 'button',
        children: [
            {
                path: 'index',
                component: React.lazy(() => import('@/views/button/index')),
                name: 'Button'
            }
        ]
    },
    {
        path: "/tag",
        redirect: '/tag/index',
        component: Layout,
        name: 'Tag',
        icon: 'tag',
        children: [
            {
                path: 'index',
                component: React.lazy(() => import('@/views/tag/index')),
                name: 'Tag'
            }
        ]
    },
    {
        path: "/table",
        redirect: '/table/index',
        component: Layout,
        name: 'Table',
        icon: 'table',
        children: [
            {
                path: 'index',
                component: React.lazy(() => import('@/views/table/index')),
                name: 'Table'
            }
        ]
    },
    {
        path: "/pagination",
        redirect: '/pagination/index',
        component: Layout,
        name: 'Pagination',
        icon: 'pagination',
        children: [
            {
                path: 'index',
                component: React.lazy(() => import('@/views/pagination/index')),
                name: 'Pagination'
            }
        ]
    },
    {
        path: "/alert",
        redirect: '/alert/index',
        component: Layout,
        name: 'Alert',
        icon: 'alert',
        children: [
            {
                path: 'index',
                component: React.lazy(() => import('@/views/alert/index')),
                name: 'Alert'
            }
        ]
    },
    {
        path: "/form",
        redirect: '/form/index',
        component: Layout,
        name: 'Form',
        icon: 'form',
        children: [
            {
                path: 'index',
                component: React.lazy(() => import('@/views/form/index')),
                name: 'Form'
            }
        ]
    },
    {
        path: "/toask",
        redirect: '/toask/index',
        component: Layout,
        name: 'Toask',
        icon: 'toask',
        children: [
            {
                path: 'index',
                component: React.lazy(() => import('@/views/toask/index')),
                name: 'Toask'
            }
        ]
    },
    {
        path: "/loading",
        redirect: '/loading/index',
        component: Layout,
        name: 'Loading',
        icon: 'loading',
        children: [
            {
                path: 'index',
                component: React.lazy(() => import('@/views/loading/index')),
                name: 'Loading'
            }
        ]
    },
    {
        path: "/crop",
        redirect: '/crop/index',
        component: Layout,
        name: 'Crop',
        icon: 'crop',
        children: [
            {
                path: 'index',
                component: React.lazy(() => import('@/views/crop/index')),
                name: 'Crop'
            }
        ]
    },
    {
        path: "/tooltip",
        redirect: '/tooltip/index',
        component: Layout,
        name: 'Tooltip',
        icon: 'tooltip',
        children: [
            {
                path: 'index',
                component: React.lazy(() => import('@/views/tooltip/index')),
                name: 'Tooltip'
            }
        ]
    },
    {
        path: "/carousel",
        redirect: '/carousel/index',
        component: Layout,
        name: 'Carousel',
        icon: 'carousel',
        children: [
            {
                path: 'index',
                component: React.lazy(() => import('@/views/carousel/index')),
                name: 'Carousel'
            }
        ]
    },
    {
        path: "/progress",
        redirect: '/progress/index',
        component: Layout,
        name: 'Progress',
        icon: 'progress',
        children: [
            {
                path: 'index',
                component: React.lazy(() => import('@/views/progress/index')),
                name: 'Progress'
            }
        ]
    },
    {
        path: "/echarts",
        redirect: '/echarts/index',
        component: Layout,
        name: 'Echarts',
        icon: 'echarts',
        children: [
            {
                path: 'index',
                component: React.lazy(() => import('@/views/echarts/index')),
                name: 'Echarts'
            }
        ]
    },
    {
        path: "/transfer",
        redirect: '/transfer/index',
        component: Layout,
        name: 'Transfer',
        icon: 'transfer',
        children: [
            {
                path: 'index',
                component: React.lazy(() => import('@/views/transfer/index')),
                name: 'Transfer'
            }
        ]
    },
    {
        path: "/notification",
        redirect: '/notification/index',
        component: Layout,
        name: 'Notification',
        icon: 'notification',
        children: [
            {
                path: 'index',
                component: React.lazy(() => import('@/views/notification/index')),
                name: 'Notification'
            }
        ]
    },
    {
        path: "/tree",
        redirect: '/tree/index',
        component: Layout,
        name: 'Tree',
        icon: 'tree',
        children: [
            {
                path: 'index',
                component: React.lazy(() => import('@/views/tree/index')),
                name: 'Tree'
            }
        ]
    },
    // {
    //     path: "/websocket",
    //     redirect: '/websocket/index',
    //     component: Layout,
    //     name: 'Websocket',
    //     icon: 'websocket',
    //     children: [
    //         {
    //             path: 'index',
    //             component: React.lazy(() => import('@/views/websocket/index')),
    //             name: 'Websocket'
    //         }
    //     ]
    // },
    // {
    //     path: "/websocket2",
    //     redirect: '/websocket2/index',
    //     component: Layout,
    //     name: 'Websocket',
    //     icon: 'websocket',
    //     children: [
    //         {
    //             path: 'index',
    //             component: React.lazy(() => import('@/views/websocket2/index')),
    //             name: 'Websocket2'
    //         }
    //     ]
    // }
]

function initRoutes(routes, parentPath = ''){
	routes.forEach(item => {
        let parent = item.parent ? [...item.parent] : []
		let fullPath = (parentPath ? (parentPath + '/') : parentPath) + item.path
        item.fullPath = fullPath
		if(!item.hidden){
            let hasChildren = (item.children !== undefined && Array.isArray(item.children) && item.children.length)
			if(hasChildren){
                let _parent = []
                parent.push({
                    path: item.path,
                    fullPath,
                    name: item.name
                })
                _parent = [...parent]
                item.children.forEach(_item => {
                    _item.parent = _parent
                    _item.parentPath = item.fullPath
                })
                initRoutes(
                    item.children,
                    fullPath
                )
			}
		}
    })
    return routes
}

export {
    routes,
    staticRoutes,
    initRoutes
}